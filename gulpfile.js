var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

gulp.task('server', function() {
	browserSync({
   	  notify: false,
   	  port: 9000,
   	  server: {
    	baseDir: 'app'
      }
   });
});   

gulp.task('watch', function(){
    gulp.watch([
        'app/assets/css/*.css',
        'app/assets/js/*.js',
        'app/assets/js/**/*.js',
        'app/views/*.html',
        'app/views/includes/*.html',
        'app/*.js',
        'app/*.html']).on('change', reload);
});

gulp.task('default', ['server', 'watch']);