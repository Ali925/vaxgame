'use strict';

angular.module('vaxApp', ['vk', 'd3', 'ui.bootstrap', 'vaxApp.directives','vaxApp.controllers', 'ngRoute', 'ngCookies'])

.config(function($routeProvider){
  $routeProvider
    .when('/', {
    templateUrl: "views/main.html"
  })
  .when('/tutorial', {
    templateUrl: "views/tutorial.html"
  })
    .when('/game', {
    templateUrl: "views/game.html"
  })
  .when('/results', {
    templateUrl: "views/results.html"
  })
  .otherwise({ 
      redirectTo: '/' 
    });
});

angular.module('vk', []);
angular.module('d3', []);
angular.module('vaxApp.directives', []);
angular.module('vaxApp.controllers', []);