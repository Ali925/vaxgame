'use strict';

angular.module('vk', []).factory('vkFactory',
    ['$document', '$rootScope', '$window', '$q',
        function($document, $rootScope, $window, $q){

        var d = $q.defer();

        var scriptTag = $document[0].createElement('script');
        scriptTag.async = true;
        scriptTag.type = 'text/javascript';
        scriptTag.src = '//vk.com/js/api/openapi.js';
        
        scriptTag.onload = function () {
            $rootScope.$apply(function(){d.resolve($window.VK)});
        };

        var b = $document[0].getElementsByTagName('body')[0];

        b.appendChild(scriptTag);
          
        return {
            VK: function(){
                return d.promise;
            }
        }

    }]);