'use strict';

angular.module('vaxApp.controllers').controller('ModalCtrl',
  ['$q', '$timeout','$scope', '$rootScope', 'vkFactory', '$location', '$uibModalInstance',
    function ($q, $timeout, $scope, $rootScope, vkFactory, $location, $uibModalInstance) {
    
        var self = this;
        
      self.close = function(){
        $uibModalInstance.close();
      };    
        
        self.tutorialClose = function(){
            $uibModalInstance.close();
            $rootScope.tutorialEnd();
        };
        
        self.resultClose = function(){
            $uibModalInstance.close();
            $rootScope.resultClose();
        };
        
        self.hardLevelClose = function(){
            $uibModalInstance.close();
            $rootScope.$broadcast('hardStart');
        };
    }
  ]);