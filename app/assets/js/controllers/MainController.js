'use strict';

angular.module('vaxApp.controllers').controller('MainCtrl',
  ['$window', '$timeout','$scope', '$rootScope', 'vkFactory', '$location', '$cookies', '$http',
    function ($window, $timeout, $scope, $rootScope, vkFactory, $location, $cookies, $http) {
      
      var self = this;
      
        self.mobile = false;
        var width = $(window).width();
        if(width<532)
            self.mobile = true;

      var now = new Date();
        var time = new Date(now.getTime() + 3600 * 1000 * 24 * 365 * 10);       
        $cookies.put('vaxGamePlayer', true, {expires: time});
          
      self.game = {};

      $scope.game = {};
      $scope.game.nextLevel = true;
      
      $scope.game.nextLevelOff = function(){
        $scope.game.nextLevel = false;
      };
      
      $scope.game.nextLevelOn = function(){
        $scope.game.nextLevel = true;
      };
      
      self.game.percents = {};
      self.game.diffPer = {};
      
      $scope.tutorialPassed = $cookies.get('tutorialPassed');
      $scope.tutorialPassedAdd = $cookies.get('tutorialPassed')
        
      self.game.tutorialInit = function(){
        if($cookies.get('diffPerm1'))
          self.game.diffPer.easy = true;
        else
            self.game.diffPer.easy = false;
        if($cookies.get('diffPerm2'))
          self.game.diffPer.normal = true;
        else
          self.game.diffPer.normal = false;  
        if($cookies.get('diffPerm3'))
          self.game.diffPer.hard = true;
        else
          self.game.diffPer.hard = false; 
      };    
      
       
        
      self.game.activeClass = function(n){
          if(n==0){
            $('.easy-icon').removeClass('easy-icon-active');
              $('.normal-icon').removeClass('normal-icon-active');
              $('.hard-icon').removeClass('hard-icon-active');
          }
        if(n==1 && self.game.diffPer.easy){
            $('.easy-icon').addClass('easy-icon-active');
            $('.normal-icon').removeClass('normal-icon-active');
              $('.hard-icon').removeClass('hard-icon-active');
        }
         else if(n==2 && self.game.diffPer.easy){
            $('.normal-icon').addClass('normal-icon-active');
             $('.easy-icon').removeClass('easy-icon-active');
             $('.hard-icon').removeClass('hard-icon-active');
         }
         else if(n==3 && self.game.diffPer.easy){
            $('.hard-icon').addClass('hard-icon-active');
             $('.easy-icon').removeClass('easy-icon-active');
              $('.normal-icon').removeClass('normal-icon-active');
         }
      };
        
      $scope.game.vkAuthed = false;
      
    
      self.game.page = 1;
      self.game.choice = undefined;
      
      self.game.startGame = function(ifNext){
        if(self.game.choice!==undefined){
            self.game.tutorialInit(); 
            if(self.game.choice==0 || self.game.choice==1 && self.game.diffPer.easy || self.game.choice==2 && self.game.diffPer.normal || self.game.choice==3 && self.game.diffPer.hard){
               if(ifNext==='next' && self.game.choice<3)
                self.game.choice = ""+(parseInt(self.game.choice)+1)+"";
              console.log(self.game.choice);
          self.game.page = 2;
          $scope.$broadcast('start-game', self.game.choice);
            }
        }
      };
      
      self.game.startNewGame = function(ifNext){
        if(self.game.choice!==undefined){
            self.game.tutorialInit(); 
            if(self.game.choice==0 || self.game.choice==1 && self.game.diffPer.easy || self.game.choice==2 && self.game.diffPer.normal || self.game.choice==3 && self.game.diffPer.hard){
               if(ifNext==='next' && self.game.choice<3)
                self.game.choice = ""+(parseInt(self.game.choice)+1)+"";
          self.game.page = 2;
                            console.log(self.game.choice);
              $location.path('/game');
                  $location.replace();
          $timeout(function(){$scope.$broadcast('start-game', self.game.choice)}, 1000);
            }
        }
      };
        
        $scope.$on('start-game', function(){});
      
      self.game.restart = function(){
        self.game.page = 1;
        self.game.choice = undefined;
      };
      
      self.game.showGameResult = function(){
        self.game.page = 3;
        $scope.$broadcast('show-diagram', self.game.percents);
      };
      
      self.game.tutorialStart = function(){
        $scope.tutorialPassed = '';
      };

      $scope.tutorialPassedChange =function(){
        $scope.tutorialPassed = 'passed';
      };
      
      $scope.$on('show-diagram', function(){});
      self.vkAuth = function(){
        vkFactory.VK().then(function(VK){
            
            VK.init({
              apiId: 5419610
            });
            
            VK.Auth.login(function(response) { 
              if (response.session) { 
                /* Пользователь успешно авторизовался */ 
                  $scope.game.vkAuthed = true;
                  $location.path('/tutorial');
                  $location.replace();
                  $scope.$apply();
                if (response.settings) { 
                  /* Выбранные настройки доступа пользователя, если они были запрошены */ 
                  console.log(response.settings);
                } 
              } else { 
                /* Пользователь нажал кнопку Отмена в окне авторизации */ 
                console.log(response);
              } 
            }, "friends,photos,offline"); 
  
          });
      };
      
      $scope.game.percentsUpdate = function(percent){
        self.game.percents = percent;
      };
      
      $scope.game.getChoice = function(){
        return self.game.choice;
      };
      
        $rootScope.tutorialEnd = function(){
            $scope.tutorialPassed = 'passed';
            $cookies.put('tutorialPassed', 'passed', {expires: time});
            $scope.$broadcast('tutorialEnd');
        };
        
        $rootScope.resultClose = function(){
            self.game.showGameResult();
        };
        
        $scope.$on('tutorialEnd', function(){});
}]);