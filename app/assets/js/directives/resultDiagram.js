'use strict';

angular.module('vaxApp.directives').directive( 'resultDiagram',
  ['d3Factory', '$q', '$window', '$location', '$cookies','$timeout','$routeParams', '$http',
    function (d3Factory, $q, $window, $location, $cookies, $timeout, $routeParams, $http) {
      
      return {
        scope: true,
        restrict: 'A',
        
        link: function($scope, $element, $attrs){
          
          d3Factory.d3().then(function(d3){ 
              $scope.result = {};
              $scope.result.text = "Your result";
              $scope.result.nextVirus = true;
              
            $scope.$on('show-diagram', function(e, d){
              var newResult;
              var oldResults;
              var oldResultsServer = $cookies.get("diffNumServer"+d.diff); 
              var now = new Date();
        var time = new Date(now.getTime() + 3600 * 1000 * 24 * 365 * 10);
              $timeout(function(){
                var height = window.innerHeight*.4;
            $('.result-diagram').css('height', height);

            oldResults = $cookies.get("diffNum"+d.diff);  
            
            $scope.result.svg = {};
            $scope.result.svg.rootNode = d3.select('.result-diagram').append('svg').attr("id", "svg-result");
                        
            var g = $scope.result.svg.rootNode
                .append("g");
            
              var coords = [];
              var percents = d, x = 10, y = 20, cy = 25, text, py = 29;
              
              var containerHeight = $('#svg-result').height()*.6;
              var containerWidth = $('#svg-result').width()*.5714;
              var heightMax = containerHeight/100;
              
            for(var i=0;i<4;i++){
              var color, height;
              if(!i){
                x=10.1;
                color = 'rgb(239,85,85)';
                height = percents.infected*heightMax;
                text = 'Инфицированы';
              }
              else if(i===1){
                x = 10;
                color = 'rgb(65,171,201)';
                height = percents.vaxed*heightMax;
                text = 'Вакцинрованы';
              }
              else if(i===2){
                x = 10;
                color = 'rgb(47,127,36)';
                height = percents.caranted*heightMax;
                text = 'На карантине';
              }
              else if(i===3){
                x = 10;
                color = 'rgb(36,89,128)';
                height = percents.uninfected*heightMax;
                text = 'Не инфицированы';
              }
              coords.push({
                x: x,
                y: y,
                height: height,
                color: color,
                cx: x+55,
                cy: cy,
                px: x+74,
                py: py,
                text: text
              });
              y=height+y;
              cy += Math.round(15.126*heightMax);
              py += Math.round(15.126*heightMax);
            }  
              
              $('.diagram-foot-text').css('top', coords[3].y+coords[3].height+25);
              $('.foot-diag-1').css('top', coords[3].y+coords[3].height-6);
              $('.foot-diag-2').css('top', coords[3].y+coords[3].height-37);
              
              var footTextH = $('.diagram-foot-text').height();
              
              g.selectAll("rect.detailed").data(coords).enter().append('rect')
                  .attr('x', function(d){return d.x;}).attr('y', function(d){return d.y;})
                  .attr('height', function(d){return d.height;}).attr('width', 35)
                  .attr('fill', function(d){return d.color;}).attr('class', 'detailed');
              
              g.selectAll('circle.title-circle').data(coords).enter().append('circle')
                .attr('cx', function(d){return d.cx;}).attr('cy', function(d){return d.cy;})
                .attr('r', 5).attr('fill', function(d){return d.color;});
              
              g.selectAll('text.title-detail').data(coords).enter().append('text')
                .attr('x', function(d){return d.px}).attr('y', function(d){return d.py})
                .text(function(d){return d.text;}).attr('fill', 'white').attr('class', 'title-detail');
              
              g.append('line').attr('x1', containerWidth).attr('y1', 20)
                  .attr('x2', containerWidth).attr('y2', coords[3].y+coords[3].height+15+footTextH)
                  .style('stroke-width', 1).style('stroke', 'white');
              
              g.append('text').text('100%').attr('fill', 'white').attr('x', containerWidth-45)
                  .attr('y', 30).attr('class', 'title-detail');
              g.append('text').text('50%').attr('fill', 'white').attr('x', containerWidth-40)
                  .attr('y', (coords[3].y+coords[3].height-20)*0.5+30).attr('class', 'title-detail');
              
              var infectPercent = Math.round(percents.infected)/100;
                  var oldInfectPercent = (100-oldResults)/100;
              var gamePercent = (100-Math.round(percents.infected))+'%';
              newResult = 100-Math.round(percents.infected);
        
              g.append('rect').attr('x', containerWidth+25).attr('y', (coords[3].y+coords[3].height-20)*infectPercent+20)
                .attr('width', 35).attr('height', containerHeight-(containerHeight*infectPercent)).attr('fill', 'rgb(36,89,128)');
              
              g.append('text').text(gamePercent).attr('fill', 'white').attr('x', containerWidth+30)
                .attr('y', (coords[3].y+coords[3].height-20)*infectPercent+10).attr('class', 'title-detail');

              if(oldResults!==undefined && oldResults>newResult){
                  g.append('rect').attr('x', containerWidth+95).attr('y', (coords[3].y+coords[3].height-20)*oldInfectPercent+20)
                .attr('width', 35).attr('height', containerHeight-(containerHeight*oldInfectPercent)).attr('fill', 'rgb(65,171,201)');
                g.append('text').text(oldResults+'%').attr('fill', 'white').attr('x', containerWidth+100)
                .attr('y', (coords[3].y+coords[3].height-20)*oldInfectPercent+10).attr('class', 'title-detail');
              }
                  else{
                      g.append('rect').attr('x', containerWidth+95).attr('y', (coords[3].y+coords[3].height-20)*infectPercent+20)
                .attr('width', 35).attr('height', containerHeight-(containerHeight*infectPercent)).attr('fill', 'rgb(65,171,201)');
                    g.append('text').text(gamePercent).attr('fill', 'white').attr('x', containerWidth+100)
                .attr('y', (coords[3].y+coords[3].height-20)*infectPercent+10).attr('class', 'title-detail');
                      $cookies.put('diffNum'+d.diff, newResult, {expires: time});
                  }
                  if(newResult>=50 || oldResults>=50){
                     $cookies.put('diffPerm'+(parseInt(d.diff)+1), true, {expires: time}); 
                    $scope.result.nextVirus = true;
                    $scope.game.nextLevelOn();
                  } else if(newResult<50 && (oldResults==undefined || oldResults<50)){
                    $scope.result.nextVirus = false;
                    $scope.game.nextLevelOff();
                  }
                  
                  if(d.diff==3){
                    $scope.result.nextVirus = false; 
                   $scope.game.nextLevelOff();
                 }
                
              });
              
              $scope.result.saveResults = function(){
                console.log(d);
                var result;
                if(oldResultsServer===undefined || oldResultsServer!==undefined && newResult>oldResultsServer){
                  result = newResult;
                  $cookies.put('diffNumServer'+d.diff, newResult, {expires: time});
                } else if(oldResultsServer!==undefined && newResult<=oldResultsServer){
                  result = oldResultsServer;
                } 
                
                var data = {
                  diff: d.diff,
                  id: d.me.uid,
                  name: d.me.first_name,
                  surname: d.me.last_name,
                  result: result
                };
                
                $http({
                  method: 'POST',
                  url: '../../results.php',
                  data: data
                }).then(function successCallback(response){
                  console.log(response);
                },function errorCallback(response){});
            };
              
            });
              
              $scope.share = {
                    vk: function() {       
                       var url  = 'http://vkontakte.ru/share.php?url=http%3A//vaxgame.netgon.ru/';
                        this.popup(url);
                    },
                    fb: function() {
                       var url  = 'https://www.facebook.com/sharer/sharer.php?u=http%3A//vaxgame.netgon.ru/';
                        this.popup(url);
                    },
                    tw: function() {
                       var url  = 'https://twitter.com/intent/tweet?url=http%3A//vaxgame.netgon.ru/';
                        this.popup(url);
                    },
                    popup: function(url) {
                        window.open(url,'','toolbar=0,status=0,width=626,height=436');
                    }
                };
            
            
            
             });
        }
      };
}]);