'use strict';

angular.module('vaxApp.directives').directive( 'vaxEditor',
  ['d3Factory', '$q', '$window', '$interval', '$document','$timeout', '$rootScope', 'vkFactory', '$uibModal',
    function (d3Factory, $q, $window, $interval, $document, $timeout, $rootScope, vkFactory, $uibModal) {
      
      return {
        scope: true,
        restrict: 'A',
        
        link: function($scope, $element, $attrs){
           $scope.$on('start-game', function(e, d){
          vkFactory.VK().then(function(VK){
              $("#svg-editor").remove();
              $("#svg-result").remove();
            
              var firstRun;
            var nodePhotos = {};
            var photosNum;
            nodePhotos.me = {};
            nodePhotos.friends = [];
            nodePhotos.friendsFriends = [];
            var friendsNum = 0;
            var friendsFriendsNum = [];
            var ids = [];
            var friendsFriends = [];
            var friendsArray = [];
            
            $scope.vkDataLoaded = undefined;
            
            if($scope.game.vkAuthed)
              VK.Auth.getLoginStatus(function authInfo(response) {
                        if (response.session) {
                          ids.push(parseInt(response.session.mid));
                          VK.api('friends.get', {user_id: response.session.mid}, function(data){
                            if(data.response && data.response.length){
                              friendsArray = data.response;
                              if(friendsArray.length>8){
                                          for(var i=0;i<9;i++)
                                              ids.push(friendsArray[i]);
                                            friendsNum = 9;
                                          
                                        } else if(friendsArray.length<=8){
                                          for(var i=0;i<friendsArray.length;i++){
                                              ids.push(friendsArray[i]);
 
                                          }
                                          friendsNum = friendsArray.length;
                                        } 
                              var t = 0, j = 0;
                                $timeout(function(){
                             var stop = $interval(function(){
                               if(j==t){
                                 j++;
                              VK.api('friends.get', {user_id: friendsArray[t]}, function(d){
                                console.log(d);
                                if(d.response && d.response.length){
                                    
                                  friendsFriends.push(d.response);
                                  
                                  if(t==friendsNum-1){
                                    console.log(t, friendsNum);
                                    for(var g=0;g<friendsNum;g++){
                                        if(friendsFriends[g].length>8){
                                          var n = 0;
                                          for(var i=0;i<9;i++){
                                            if(ids.indexOf(friendsFriends[g][i])==-1){
                                              ids.push(friendsFriends[g][i]);
                                              n++;
                                            }
                                          }
                                          friendsFriendsNum.push(n);
                                        } else if(friendsFriends[g].length<=8){
                                         var n = 0;
                                          for(var i=0;i<friendsFriends[g].length;i++){
                                            if(ids.indexOf(friendsFriends[g][i])==-1){
                                              ids.push(friendsFriends[g][i]);
                                              n++;
                                            }
                                          }
                                          friendsFriendsNum.push(n);
                                        }
                                    }
                                    $interval.cancel(stop);
                                     VK.api('users.get', {uids: ""+ids.join()+"", fields: 'photo'}, function(photos){
                            photosNum = photos.response.length;           
                            nodePhotos.me = photos.response[0];
                            for(var i=1;i<=friendsNum;i++){
                              nodePhotos.friends.push(photos.response[i]);
                            }           
                            var startPoint = friendsNum+1;           
                            for(var i=0;i<friendsFriendsNum.length;i++){
                              nodePhotos.friendsFriends[i] = [];
                              for(var g=startPoint;g<friendsFriendsNum[i]+startPoint;g++){
                                nodePhotos.friendsFriends[i].push(photos.response[g]);
                              }
                              startPoint += friendsFriendsNum[i];
                            }         
                            $scope.vkDataLoaded = 'loaded';
                            $scope.$apply();         
                            console.log(nodePhotos);
                  });
                                }
                                  t++;
                            }
                                
                              });
                             }
                            },340);
                            }, 1000);
                            }
                            else{
                              VK.api('users.get', {uids: ""+ids.join()+"", fields: 'photo'}, function(photos){
                            photosNum = photos.response.length;
                            nodePhotos.me = photos.response[0];    
                            $scope.vkDataLoaded = 'loaded';
                            $scope.$apply();
                  });
                            }
                          });
                          
                        } else {
                          $scope.vkDataLoaded = 'noloaded';
                        }
                      });
            else
              $scope.vkDataLoaded = 'noloaded';
          
        $scope.$watch('vkDataLoaded', function(newVar, oldVar){
          if(newVar!==undefined && firstRun===undefined)   
          d3Factory.d3().then(function(d3){
              var height = window.innerHeight;
            var width = window.innerWidth*.75;
            
            $scope.editor = {};
            $scope.editor.finish = "";
            
            
            $scope.editor.svg = {};
            $scope.editor.svg.rootNode = d3.select($element[0]).append('svg').attr("id", "svg-editor")
                                            .attr("width", width)
                                            .attr("height", height);
            
            var tip = d3.tip()
                  .html(function(d){
                    if(d!==undefined && d.infected){
                      $timeout(function(){changeBackground('infected-text');});
                      return "<p class='infected-text'>"+d.name.first_name + " " + d.name.last_name+"</p><p>инфицирован</p>";
                    }
                    else if(d!==undefined && d.immun){
                      $timeout(function(){changeBackground('immun-text');});
                      return "<p class='immun-text'>"+d.name.first_name + " " + d.name.last_name+"</p><p>иммунирован</p>";
                    }
                    else if(d!==undefined && d.refuser){
                      $timeout(function(){changeBackground('refuser-text');});
                      return "<p class='refuser-text'>"+d.name.first_name + " " + d.name.last_name+"</p><p>отказал</p>";
                    }
                    else{
                      $timeout(function(){changeBackground('d3tip-text');});
                      return "<p>"+d.name.first_name + " " + d.name.last_name+"</p>";
                    }
                  })
                  .direction('n')
                  .offset([-10, 0])
                  .attr('class', 'd3-tip');
                            
                        
            var g = $scope.editor.svg.rootNode
                .append("g");
            
            g.call(tip);

            $scope.editor.svg.underlay = g.append("rect")
                .attr("class", "underlay")
                .attr("width", "100%")
                .attr("height", "100%");


            $scope.editor.carant = 0;
            var infection = 0;
            var finishGame = false, finishKey = 0;
            var dragged = false;
            var diffucilty = d;
            $scope.editor.diffucilty = parseInt(d);
            var immuns = [];  
            var coords = [];
            var coordsFixed = [];
            var links = [];
            
            var charge, friction, linkDistance, gravity, roundNum, radiusS, radiusE, startNum, maxInfNodes, maxInfLinks, cureOn=false, immunOn=false, changeOn=false, dragTime, clickedOn=false, vaxNum, hardLevelStart = false, isMouseOver = false;
            
            switch(diffucilty){
              case '0':
                 charge = -1000;
                 roundNum = 40;
                 linkDistance = 60;
                 radiusS = 6;
                 radiusE = 13;
                 $scope.editor.vax = 5;
                 $scope.editor.vaxNum = 5;
                 $scope.editor.cure = 0;
                 $scope.editor.immun = 0;
                 $scope.editor.changes = 0;
                 startNum = 1;
                 maxInfNodes = 1;
                 maxInfLinks = 1;
                 break;
              case '1':
                 charge = -900;
                 roundNum = 60;
                 linkDistance = 60;
                 radiusS = 7;
                 radiusE = 15;
                 $scope.editor.vax = 7;
                 $scope.editor.vaxNum = 7;
                 $scope.editor.cure = 1;
                 $scope.editor.cureNum = 1;
                 $scope.editor.immun = 0;
                 $scope.editor.changes = 0;
                 startNum = 1;
                 maxInfNodes = 1;
                 maxInfLinks = 2;
                 break;
              case '2':
                 charge = -750;
                 roundNum = 80;
                 linkDistance = 60;
                 radiusS = 9;
                 radiusE = 17;
                 $scope.editor.vax = 10;
                 $scope.editor.vaxNum = 10;
                 $scope.editor.cure = 1;
                 $scope.editor.cureNum = 1;
                 $scope.editor.immun = 1;
                 $scope.editor.immunNum = 1;
                 $scope.editor.changes = 1;
                 $scope.editor.changesNum = 1;
                 startNum = 2;
                 maxInfNodes = 2;
                 maxInfLinks = 2;
                 break;
              case '3':
                 charge = -600;
                 roundNum = 100;
                 linkDistance = 40;
                 radiusS = 11;
                 radiusE = 19;
                 $scope.editor.vax = 15;
                 $scope.editor.cure = 2;
                 $scope.editor.immun = 1;
                 $scope.editor.changes = 2;
                 $scope.editor.vaxNum = 15;
                 $scope.editor.cureNum = 2;
                 $scope.editor.immunNum = 1;
                 $scope.editor.changesNum = 2;
                 startNum = 3;
                 maxInfNodes = 2;
                 maxInfLinks = 3;
                 break;
            }
            vaxNum = $scope.editor.vax;
              
              $scope.editor.cureOn = function(){
                if($scope.editor.cure>0 && !cureOn && !immunOn && !changeOn && $scope.editor.vax<-2){
                  $scope.editor.cure--;
                  cureOn = true;
                }
              };
              $scope.editor.immunOn = function(){
                if($scope.editor.immun>0 && !immunOn && !changeOn && !cureOn && $scope.editor.vax<-2){
                  $scope.editor.immun--;
                  immunOn = true;
                }
              };
              $scope.editor.changeOn = function(){
                if($scope.editor.changes>0 && !changeOn && !cureOn && !immunOn && $scope.editor.vax<-2){
                  $scope.editor.changes--;
                  changeOn = true;
                }
              };
            
            for(var i=0;i<roundNum;i++){
              coords.push({
                x: Math.floor(Math.random()*width)+1,
                y: Math.floor(Math.random()*height)+1,
                r: Math.floor(Math.random()*(radiusE-radiusS+1))+radiusS,
                infected: false,
                color: 'white',
                stroke: 'black',  
                immun: false,
                refuser: false
              });
              
              if(i<(roundNum-1)){
                links.push({
                  source:i,
                  target:i+1
                });
              }
              else{
                links.push({
                  source:i,
                  target:0
                });
              }
              
              if(i%2==0&&i<(roundNum-15)){
                links.push({
                  source:i,
                  target:i+15
                });
              }
              
              if(i%2==1 && i<(roundNum-13)){
                links.push({
                  source:i,
                  target:i+13
                });
              }
              
              if(i%3==1 && i<(roundNum-16)){
                links.push({
                  source:i,
                  target:i+14
                });
              }
              
              if(i%5==1 && i<(roundNum-5)){
                links.push({
                  source:i,
                  target:i+5
                });
              }
              
              if(i%5==1 && i<(roundNum-20)){
                links.push({
                  source:i,
                  target:i+20
                });
              }
              
            }
              
              
            var web = d3.layout.force();
            
              web.friction(.83)
                  .linkDistance(linkDistance)
                  .charge(charge) 
                  .on("tick", tick);
            
            
            web.size([width, height]);
              
            
            var svgLinks = g.selectAll("line").data(links)
                      .enter().append("line").style("stroke", "white").style("stroke-width", 2);
            
            var radiusBigger = false;
            
            
             var circles = g.selectAll("circle")
              .data(coords);
            
            circles.enter().append("circle").attr("fill", function(d, i){return d.color;})
              .style("stroke", 'black')
              .attr("r", function(d, i){return d.r;}).attr("class", "node").attr('data-index', function(d, i){return i;})
              .on("mouseover", function(d, i){
              
              d3.select(circles[0][i]).attr('class', 'node negative');
  
            }).on("mouseout", function(d, i){
              d3.select(circles[0][i]).attr('class', 'node');
              
            })
              .on("click", function(d, t){
              if(!dragged && !clickedOn && !$scope.editor.finish){
                if(!cureOn && !immunOn && !changeOn && !d.infected && !d.immun){
                  if(!d.refuser || $scope.editor.vax<=0){
                    clickedOn = true;
                 $scope.editor.vax--;
                  $scope.$apply();  
              var i = 0, removedLines = [], n=0,m;
              while (i < links.length) {
                if ((links[i].source.index == d.index) || (links[i].target.index == d.index)) {
                  removedLines.push(i);
                }
                i++;
             }
             
    
              coords.splice(d.index, 1);       
             
              for(var i=0;i<removedLines.length;i++){
                m=removedLines[i];
                if(n>0)
                  m=m-n;
                links.splice(m, 1);
                n++;
              }
                    updateWeb();
                  }
                }
                else if(cureOn){
                  if(coords[d.index].infected || coords[d.index].refuser){
                    if($scope.editor.vax<0)
                      $scope.editor.vax--;
                    clickedOn = true;
                    var i = 0, removedLines = [], n=0,m;
                    while (i < links.length) {
                      if ((links[i].source.index == d.index) || (links[i].target.index == d.index)) {
                        removedLines.push(i);
                      }
                      i++;
                   }
                    
                    
                    coords.splice(d.index, 1); 
             
                    for(var i=0;i<removedLines.length;i++){
                      m=removedLines[i];
                      if(n>0)
                        m=m-n;
                      links.splice(m, 1);
                      n++;
                    }
                    cureOn = false;
                    updateWeb();
                  }
                }
                else if(immunOn){
                  if(!coords[d.index].infected && !coords[d.index].immun){
                    if($scope.editor.vax<=0)
                      $scope.editor.vax--;
                    clickedOn = true;
                    immuns.push({
                      index: d.index,
                      step: 3
                    });  
                    coords[d.index].immun = true;
                    coords[d.index].color = 'blue';
                    coords[d.index].immunNum = d.index;
                    immunOn = false;
                    updateWeb();
                  }
                }
                else if(changeOn){
                  if(coords[d.index].infected && !coords[d.index].immun || coords[d.index].refuser){
                    if($scope.editor.vax<0)
                      $scope.editor.vax--;
                    clickedOn = true;
                    coords[d.index].infected = false;
                    coords[d.index].color = 'white';
                    coords[d.index].refuser = false;
                    changeOn = false;
                    updateWeb();
                  }
                }
              
              }
              dragged=false;
            });
            
            if($scope.vkDataLoaded=="loaded"){
              var clipPath = g.selectAll('clipPath').data(coords).enter().append('clipPath');
              
              var cirlcePhotos = g.selectAll("ciclePhoto").data(coords).enter().append("image");
            }

            
            var drag = web.drag()
            .on("dragstart", function(){
              dragTime = new Date(); 
              dragged=true;
            })
            .on("dragend", function(){
              var finishTime = new Date();
              if((finishTime-dragTime)<250)
                dragged = false;
            });
            
            circles.call(drag);
            if($scope.vkDataLoaded=="loaded")
              cirlcePhotos.call(drag);
            
            web.nodes(coords)
                .links(links)
                .start();
            
             var webLinks = web.links();
            var webNodes = web.nodes();
            
            if($scope.vkDataLoaded=="loaded"){
            
            var startPoint, friendsQ=[], friendsFriendsQ = [], quantity = 1;
              if(diffucilty==0)
                startPoint = 6;
              else if(diffucilty==1)
                startPoint = 31;
              else if(diffucilty==2)
                startPoint = 36;
              else if(diffucilty==3)
                startPoint = 46;
              
            coords[startPoint].color = "#DCDCDC";
              coords[startPoint].stroke = "transparent";
            coords[startPoint].name = {
              first_name: "Вы",
              last_name: "",
              photo: nodePhotos.me.photo
            };
              coords[startPoint].r = 22;
            
              
              for(var i=0;i<links.length;i++){
                if(links[i].target.index == coords[startPoint].index && friendsQ.length<nodePhotos.friends.length){                  
                  quantity++;
                  coords[links[i].source.index].name = nodePhotos.friends[friendsQ.length];
                  coords[links[i].source.index].r = 17;
                  coords[links[i].source.index].color = "#DCDCDC";
                  coords[links[i].source.index].stroke = "transparent";
                  friendsQ.push(coords[links[i].source.index]);                                                                          
                } 
                else if(links[i].source.index == coords[startPoint].index && friendsQ.length<nodePhotos.friends.length){
                  quantity++;
                  coords[links[i].target.index].name = nodePhotos.friends[friendsQ.length];
                  coords[links[i].target.index].r = 17;
                  coords[links[i].target.index].color = "#DCDCDC";
                  coords[links[i].target.index].stroke = "transparent";
                  friendsQ.push(coords[links[i].target.index]);
                }
              }
            
            for(var i=0;i<friendsQ.length;i++){
              friendsFriendsQ[i] = 0;
                  for(var j=0;j<links.length;j++){
                    if(links[j].target.index == friendsQ[i].index && friendsFriendsQ[i]<nodePhotos.friendsFriends[i].length && links[j].source.index!=startPoint && friendsQ.indexOf(coords[links[j].source.index])==-1){
                      quantity++;
                      coords[links[j].source.index].name = nodePhotos.friendsFriends[i][friendsFriendsQ[i]];
                      coords[links[j].source.index].r = 17;
                      coords[links[j].source.index].color = "#DCDCDC";
                      coords[links[j].source.index].stroke = "transparent";
                      friendsFriendsQ[i]++;
                    }
                    else if(links[j].source.index == friendsQ[i].index && friendsFriendsQ[i]<nodePhotos.friendsFriends[i].length && links[j].target.index!=startPoint && friendsQ.indexOf(coords[links[j].source.index])==-1){
                      quantity++;
                      coords[links[j].target.index].name = nodePhotos.friendsFriends[i][friendsFriendsQ[i]];
                      coords[links[j].target.index].r = 17;
                      coords[links[j].target.index].color = "#DCDCDC";
                      coords[links[j].target.index].stroke = "transparent";
                      friendsFriendsQ[i]++;
                    }
                  }             
            }
              clipPath.attr('id', function(d) { return "clip" + d.index; });
              
              var clipPathCircles = clipPath.append('circle').attr('r', function(d, i){return (d.r-2);})
                                      .attr('class', 'pathCircle');
                        

              cirlcePhotos.attr('xlink:href', function(d, i){
                          if(d.name!=undefined)
                            return d.name.photo;
                          else
                            return "";
                        })
                        .attr('clip-path', function(d, i){ return "url(#clip" + d.index + ")"; })
                        .attr('width', function(d, i){return (d.r*2);})
                        .attr('height', function(d, i){return (d.r*2);})
                        .on("mouseover", function(d, i){
              
              d3.select(circles[0][d.index]).attr('class', 'node negative');
              if(d.name && !dragged){
                tip.show(d);
                  if(!radiusBigger){
                
           
                          coords[d.index].r =  coords[d.index].r*1.4; 
                          
                    
                    for(var y=0;y<coords.length;y++){
                  d3.select(circles[0][y])
                  .attr('r', function(d, i){
                          return d.r;
                  });
                      d3.select(cirlcePhotos[0][y])
                  .attr('width', function(d, i){
                          return d.r*2;
                  })
                      .attr('height', function(d, i){
                          return d.r*2;
                  })
                        .attr('x', function(d){return (d.x-d.r);})
                      .attr('y', function(d){return (d.y-d.r);});
                      
                      d3.select(clipPathCircles[0][y])
                  .attr('r', function(d, i){
                          return (d.r-2);
                  });
                    }
                  
                      radiusBigger = true;
                  }

                }
  
            }).on("mouseout", function(d, i){
              d3.select(circles[0][d.index]).attr('class', 'node');
              if(d.name) {

                tip.hide();

                  if(radiusBigger){
       
                              coords[d.index].r =  coords[d.index].r/1.4; 
                          
                    
                     for(var y=0;y<coords.length;y++){
                  d3.select(circles[0][y])
                  .attr('r', function(d, i){return d.r;});
                        d3.select(cirlcePhotos[0][y])
                  .attr('width', function(d, i){return d.r*2;})
                        .attr('height', function(d, i){return d.r*2;})
                        .attr('x', function(d){return (d.x-d.r);})
                      .attr('y', function(d){return (d.y-d.r);});
                       
                        d3.select(clipPathCircles[0][y])
                  .attr('r', function(d, i){return (d.r-2);});
                     }
                    
                      radiusBigger = false;
                      }  
              }
              
            })
              .on("click", function(d, t){
              if(!dragged && !clickedOn && !$scope.editor.finish){
                if(!cureOn && !immunOn && !changeOn && !d.infected && !d.immun){
                  if(!d.refuser || $scope.editor.vax<=0){
                    clickedOn = true;
                 $scope.editor.vax--;
                  $scope.$apply();  
              var i = 0, removedLines = [], n=0,m;
              while (i < links.length) {
                if ((links[i].source.index == d.index) || (links[i].target.index == d.index)) {
                  removedLines.push(i);
                }
                i++;
             }
             
    
              coords.splice(d.index, 1);       
             
              for(var i=0;i<removedLines.length;i++){
                m=removedLines[i];
                if(n>0)
                  m=m-n;
                links.splice(m, 1);
                n++;
              }
                    if(radiusBigger){ 
                       tip.hide();
  
                   
                    radiusBigger = false;
                 }
        
                    updateWeb();
                  }
                }
                else if(cureOn){
                  if(coords[d.index].infected || coords[d.index].refuser){
                    if($scope.editor.vax<0)
                      $scope.editor.vax--;
                    clickedOn = true;
                    var i = 0, removedLines = [], n=0,m;
                    while (i < links.length) {
                      if ((links[i].source.index == d.index) || (links[i].target.index == d.index)) {
                        removedLines.push(i);
                      }
                      i++;
                   }
                    
                    
                    coords.splice(d.index, 1); 
             
                    for(var i=0;i<removedLines.length;i++){
                      m=removedLines[i];
                      if(n>0)
                        m=m-n;
                      links.splice(m, 1);
                      n++;
                    }
                    cureOn = false;
                    
                    if(radiusBigger){ 
                      tip.hide();
                      for(var coord in coords){
                  coords[coord].tipShown = false;
                }
                    for(var y=0;y<coords.length;y++){
                        if(coords[y].name!=undefined)
                            coords[y].r =  coords[y].r/1.4; 
                        }
                    radiusBigger = false;
                    
                 };
            
                    updateWeb();
                  }
                }
                else if(immunOn){
                  if(!coords[d.index].infected && !coords[d.index].immun){
                    if($scope.editor.vax<=0)
                      $scope.editor.vax--;
                    clickedOn = true;
                    immuns.push({
                      index: d.index,
                      step: 3
                    });  
                    coords[d.index].immun = true;
                    coords[d.index].color = 'blue';
                    coords[d.index].immunNum = d.index;
                    immunOn = false;
                    updateWeb();
                  }
                }
                else if(changeOn){
                  if(coords[d.index].infected && !coords[d.index].immun || coords[d.index].refuser){
                    if($scope.editor.vax<0)
                      $scope.editor.vax--;
                    clickedOn = true;
                    coords[d.index].infected = false;
                    if(coords[d.index].name!==undefined)
                      coords[d.index].color = "#DCDCDC";
                    else
                      coords[d.index].color = 'white';
                    coords[d.index].refuser = false;
                    changeOn = false;
                    updateWeb();
                  }
                }
              
              }
              dragged=false;
            });
            }
            
            if(diffucilty==3){
              
              var count = Math.floor(Math.random()*5)+3;
              
              var nums = [];
              
              for(var i=0;i<count;i++){
                var num = Math.floor(Math.random()*100);
                var a = nums.indexOf(num);
                if(a==-1)
                  nums.push(num);
                else
                  i--;
              }
              
              for(var i=0;i<nums.length;i++){
                coords[nums[i]].color = "orange";
                coords[nums[i]].refuser = true;
              }
              
            }
              
              if(diffucilty==0)
                  $uibModal.open({
                          animation: true,
                          templateUrl: '../../views/includes/modalVacWindow.html',
                          controller: 'ModalCtrl',
                          controllerAs: 'modal',
                          size: 500,
                          backdrop: 'static',
                          resolve: {
                            items: function () {
                              return $scope.items;
                            }
                          }
                        });
              else if(diffucilty==3)
                  $uibModal.open({
                          animation: true,
                          templateUrl: '../../views/includes/modalRefuserWindow.html',
                          controller: 'ModalCtrl',
                          controllerAs: 'modal',
                          size: 500,
                          backdrop: 'static',
                          resolve: {
                            items: function () {
                              return $scope.items;
                            }
                          }
                        });
            
            function tick(e){
              circles.each(gravity(0.01 * e.alpha))
                    .attr("cx", function(d){
                        return d.x = Math.max((d.r+3), Math.min(width - d.r - 3, d.x));
                    })
                      .attr("cy", function(d){
                     return d.y = Math.max((d.r+3), Math.min(height - d.r - 3, d.y));
                    }).attr("fill", function(d, i){
                      if(d3.select(circles[0][i]).attr("fill")==d.image)
                        return d.image;
                      else
                        return d.color;
                    })
                      .attr("r", function(d, i){return d.r;})
                      .style("stroke", function(d, i){return d.stroke});
              if($scope.vkDataLoaded=="loaded"){
              cirlcePhotos.each(gravity(0.01 * e.alpha))
                    .attr("x", function(d){
                        return (d.x-d.r);
                    })
                      .attr("y", function(d){
                     return (d.y-d.r);
                    })
                      .attr("width", function(d){
                        return (d.r*2);
                      })
                      .attr("height", function(d){
                        return (d.r*2);
                      });
              
              clipPathCircles.each(gravity(0.01 * e.alpha))
                    .attr("cx", function(d){
                        return d.x = Math.max((d.r+3), Math.min(width - d.r - 3, d.x));
                    })
                      .attr("cy", function(d){
                     return d.y = Math.max((d.r+3), Math.min(height - d.r - 3, d.y));
                    })
                      .attr('r', function(d){return (d.r-2);});
              }
              
              
              svgLinks.attr('x1', function(d){return d.source.x;})
                      .attr('y1', function(d){return d.source.y;})
                      .attr('x2', function(d){return d.target.x;})
                      .attr('y2', function(d){return d.target.y;});
            }
              
              function updateWeb(){
                  
                  if(diffucilty==1 && $scope.editor.vax==-3)
                      $uibModal.open({
                          animation: true,
                          templateUrl: '../../views/includes/modalCureWindow.html',
                          controller: 'ModalCtrl',
                          controllerAs: 'modal',
                          size: 500,
                          backdrop: 'static',
                          resolve: {
                            items: function () {
                              return $scope.items;
                            }
                          }
                        });
                  else if(diffucilty==2 && $scope.editor.vax==-3){
                    $uibModal.open({
                          animation: true,
                          templateUrl: '../../views/includes/modalImmunWindow.html',
                          controller: 'ModalCtrl',
                          controllerAs: 'modal',
                          size: 500,
                          backdrop: 'static',
                          resolve: {
                            items: function () {
                              return $scope.items;
                            }
                          }
                        });
                      $uibModal.open({
                          animation: true,
                          templateUrl: '../../views/includes/modalReplaceWindow.html',
                          controller: 'ModalCtrl',
                          controllerAs: 'modal',
                          size: 500,
                          backdrop: 'static',
                          resolve: {
                            items: function () {
                              return $scope.items;
                            }
                          }
                        });
                  }

              var randomStart = [];
              var infectedNodes = [];
              var infectedLinks = [];
              var infectedAll = [];
              var infectedLinkOpt;
              var found = true, item, link, itemNum, infCount = 0;
              
              if(!$scope.editor.vax){
                
                for(var i=0;i<startNum;i++){
                  var random = Math.floor(Math.random()*coords.length);
                  if(randomStart.indexOf(random)==-1 && !coords[random].immun){
                    randomStart.push(random);
                    coords[random].infected = true;
                    coords[random].color = 'red';
                  }
                  else
                    i--;
                }
                
              }
              else if($scope.editor.vax<0 && diffucilty<3){
                
                $scope.editor.carant++;
                $scope.$apply();  
                
                for(var i=0;i<coords.length;i++){
                  if(coords[i].infected)
                    infectedNodes.push(coords[i]);
                }
                
                for(var k=maxInfNodes;k>0;k--){
                  if(infectedNodes.length)
                  found=true;
                while(found){
                  var randomNum = Math.floor(Math.random()*infectedNodes.length);
                  item = infectedNodes[randomNum];
                  for(var i=0;i<links.length;i++){
                    if((links[i].source.index == item.index && !links[i].target.infected && !links[i].target.immun) || (links[i].target.index == item.index && !links[i].source.infected && !links[i].source.immun)){
                      infectedLinks.push(links[i]);
                      found = false;
                    }
                  }
                  
                  
                  for(var t=0;t<maxInfLinks;t++){
                    var key = true;
                    var randomLink = Math.floor(Math.random()*infectedLinks.length);
                    link = infectedLinks[randomLink];
                    
                    for(var x=0;x<infectedAll.length;x++){
                      if(infectedAll[x].link==link){
                        key=false;
                        break;
                      }
                    }
                    
                    if(!found && key){
                      infectedAll.push({
                        'item': item,
                        'link': link
                      });
                      infectedNodes.splice(randomNum, 1);
                    }
                    else if(!key && infectedLinks.length){
                      t--;
                      infectedLinks.splice(randomLink, 1);
                    }
                    if(!infectedLinks.length)
                      break;
                  }
                  
                  if(found && infectedNodes.length)
                    infectedNodes.splice(randomNum, 1);
                  if(found && !infectedNodes.length){
                    found=false;
                  }
                }

                }
                
                if(infectedAll.length){
                  for(var j=0;j<infectedAll.length;j++){
                  if(infectedAll[j].link.source.index == infectedAll[j].item.index){
                    infectedAll[j].infectedLinkOpt = 'source';
                    for(var i=0;i<coords.length;i++){
                      if(coords[i].index == infectedAll[j].link.target.index){
                        infectedAll[j].itemNum=i;
                      }
                    }
                  } else{
                    infectedAll[j].infectedLinkOpt = 'target';
                    for(var i=0;i<coords.length;i++){
                      if(coords[i].index == infectedAll[j].link.source.index){
                        infectedAll[j].itemNum=i;
                      }
                    }
                  }
                }
                }
                  
                  if(!infectedAll.length && $scope.editor.vax<0)
                      finishGame = true;
              }
              if($scope.editor.vax<=0 && diffucilty==3 && !hardLevelStart){
                  $uibModal.open({
                          animation: true,
                          templateUrl: '../../views/includes/modalHardLevelWindow.html',
                          controller: 'ModalCtrl',
                          openedClass: 'modal-quarant modal-hard-level',
                          controllerAs: 'modal',
                          size: 500,
                          backdrop: 'static',
                          resolve: {
                            items: function () {
                              return $scope.items;
                            }
                          }
                        });
                  $rootScope.$on('hardStart', function(){
                    hardLevelStart = true;
                  $scope.game.showTimeout = true;
                  var time = Date.now()+11000;
                  var timeoutStop = $interval(function(){
                      var now = Date.now();
                      if((time-now)/1000>=9.5)
                        $scope.game.timeoutText = '00:'+Math.round((time-now)/1000);
                      else if(time-now>0)
                        $scope.game.timeoutText = '00:0'+Math.round((time-now)/1000); 
                      else {
                          $scope.game.showTimeout = false;
                          $interval.cancel(timeoutStop);
                      }
                  }, 1000);
                  
                  $timeout(function(){
                var hardGame = $interval(function(){
                    infectedNodes = [];
                      infectedLinks = [];
                      infectedAll = [];  
                    
                    $scope.editor.carant++;
                
                for(var i=0;i<coords.length;i++){
                  if(coords[i].infected)
                    infectedNodes.push(coords[i]);
                }
                
                for(var k=maxInfNodes;k>0;k--){
                  if(infectedNodes.length)
                  found=true;
                while(found){
                  var randomNum = Math.floor(Math.random()*infectedNodes.length);
                  item = infectedNodes[randomNum];
                  for(var i=0;i<links.length;i++){
                    if((links[i].source.index == item.index && !links[i].target.infected && !links[i].target.immun) || (links[i].target.index == item.index && !links[i].source.infected && !links[i].source.immun)){
                      infectedLinks.push(links[i]);
                      found = false;
                    }
                  }
                  
                  
                  for(var t=0;t<maxInfLinks;t++){
                    var key = true;
                    var randomLink = Math.floor(Math.random()*infectedLinks.length);
                    link = infectedLinks[randomLink];
                    
                    for(var x=0;x<infectedAll.length;x++){
                      if(infectedAll[x].link==link){
                        key=false;
                        break;
                      }
                    }
                    
                    if(!found && key){
                      infectedAll.push({
                        'item': item,
                        'link': link
                      });
                      infectedNodes.splice(randomNum, 1);
                    }
                    else if(!key && infectedLinks.length){
                      t--;
                      infectedLinks.splice(randomLink, 1);
                    }
                    if(!infectedLinks.length)
                      break;
                  }
                  
                  if(found && infectedNodes.length)
                    infectedNodes.splice(randomNum, 1);
                  if(found && !infectedNodes.length){
                    found=false;
                  }
                }

                }
                
                if(infectedAll.length){
                  for(var j=0;j<infectedAll.length;j++){
                  if(infectedAll[j].link.source.index == infectedAll[j].item.index){
                    infectedAll[j].infectedLinkOpt = 'source';
                    for(var i=0;i<coords.length;i++){
                      if(coords[i].index == infectedAll[j].link.target.index){
                        infectedAll[j].itemNum=i;
                      }
                    }
                  } else{
                    infectedAll[j].infectedLinkOpt = 'target';
                    for(var i=0;i<coords.length;i++){
                      if(coords[i].index == infectedAll[j].link.source.index){
                        infectedAll[j].itemNum=i;
                      }
                    }
                  }
                }
                    
                for(var j=0;j<infectedAll.length;j++){
                  if(infectedAll[j].infectedLinkOpt=='source')
                    infectedAll[j].infectedLink = infectedAll[j].link.target;
                  else
                    infectedAll[j].infectedLink = infectedAll[j].link.source;
                }
                g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                  .attr("transform", function(d, i){
                      return "translate("+infectedAll[i].item.x+", "+infectedAll[i].item.y+")";
                    })
                  .attr("class", "infection");
                g.selectAll("circle.infection").transition()
                      .duration(500)
                  .attr("transform", function(d, i){
                      return "translate("+infectedAll[i].infectedLink.x+", "+infectedAll[i].infectedLink.y+")";
                  })
                  .remove();
                
              }
                    
                    if(!infectedAll.length && $scope.editor.vax<0)
                      finishGame = true;
                    
                    if(immuns.length){
                for(var i=0;i<immuns.length;i++){
                  immuns[i].step--;
                  if(immuns[i].step<0){
                    for(var j=0;j<coords.length;j++){
                      if(coords[j].immunNum == immuns[i].index){
                        coords[j].immun = false;
                        coords[j].color = 'white';
                        break;
                      }
                    }
                    
                  }
                }
                
              }

              
              g.selectAll("circle.node").data(coords, function(d){return d.index;}).exit().remove();
                  g.selectAll("image").data(coords, function(d){return d.index;}).exit().remove();
                g.selectAll("clippath").data(coords, function(d){return d.index;}).exit().remove();
              g.selectAll("line").data(links, function(d){return d.source.index+"-"+d.target.index;}).exit().remove();
              
              
              web.nodes(coords)
                .links(links)
                .start();
              
              if($scope.editor.vax<=0){
              $timeout(function(){
                for(var j=0;j<infectedAll.length;j++){
                  coords[infectedAll[j].itemNum].infected = true;
                  coords[infectedAll[j].itemNum].color = 'red';
                }
                
                if(infectedAll.length && $scope.editor.vax<0){
                
                infectedNodes = [];
                finishGame = true;
                
                for(var i=0;i<coords.length;i++){
                  if(coords[i].infected)
                    infectedNodes.push(coords[i]);
                }
                

                for(var o=0;o<infectedNodes.length;o++){
                  for(var i=0;i<links.length;i++){
                    if((links[i].source.index == infectedNodes[o].index && !links[i].target.infected && !links[i].target.immun) || (links[i].target.index == infectedNodes[o].index && !links[i].source.infected && !links[i].source.immun)){
                      finishGame = false;
                      break;
                    } 
                  }
                }
              }
                
              if(finishGame && !finishKey){
                  $interval.cancel(hardGame);
                  finishKey=1;
                var infectedNodes = 0;
                var uninfectedNodes = 0;
                for(var i=0;i<coords.length;i++){
                  if(coords[i].infected)
                    infectedNodes++;
                  else
                    uninfectedNodes++;
                }
                
                var percents = {
                  infected: infectedNodes/roundNum*100,
                  vaxed: vaxNum/roundNum*100,
                  caranted: $scope.editor.carant/roundNum*100,
                  uninfected: uninfectedNodes/roundNum*100,
                  diff: diffucilty,
                  me: nodePhotos.me
                };
                
                $scope.game.percentsUpdate(percents);
                $uibModal.open({
                          animation: true,
                          templateUrl: '../../views/includes/modalResultsWindow.html',
                          controller: 'ModalCtrl',
                          openedClass: 'modal-quarant',
                          controllerAs: 'modal',
                          size: 500,
                          backdrop: 'static',
                          resolve: {
                            items: function () {
                              return $scope.items;
                            }
                          }
                        });
                $scope.editor.finish = 'Congrats! You finish game!'; 
              }
                clickedOn = false;
                }, 600);} else{
                clickedOn = false;
              }
                }, 1000);
                  }, 10000);
              });
              }
              
              
              if(infectedAll.length){
                for(var j=0;j<infectedAll.length;j++){
                  if(infectedAll[j].infectedLinkOpt=='source')
                    infectedAll[j].infectedLink = infectedAll[j].link.target;
                  else
                    infectedAll[j].infectedLink = infectedAll[j].link.source;
                }
                g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                  .attr("transform", function(d, i){
                      return "translate("+infectedAll[i].item.x+", "+infectedAll[i].item.y+")";
                    })
                  .attr("class", "infection");
                g.selectAll("circle.infection").transition()
                      .duration(500)
                  .attr("transform", function(d, i){
                      return "translate("+infectedAll[i].infectedLink.x+", "+infectedAll[i].infectedLink.y+")";
                  })
                  .remove();
                
              }
              
              if(immuns.length){
                for(var i=0;i<immuns.length;i++){
                  immuns[i].step--;
                  if(immuns[i].step<0){
                    for(var j=0;j<coords.length;j++){
                      if(coords[j].immunNum == immuns[i].index){
                        coords[j].immun = false;
                        coords[j].color = 'white';
                        break;
                      }
                    }
                    
                  }
                }
                
              }

              
              g.selectAll("circle.node").data(coords, function(d){return d.index;}).exit().remove();
                g.selectAll("image").data(coords, function(d){return d.index;}).exit().remove();
                g.selectAll("clippath").data(coords, function(d){return d.index;}).exit().remove();
              g.selectAll("line").data(links, function(d){return d.source.index+"-"+d.target.index;}).exit().remove();
              
              
              web.nodes(coords)
                .links(links)
                .start();
              
              if($scope.editor.vax<0){
              $timeout(function(){
                for(var j=0;j<infectedAll.length;j++){
                  coords[infectedAll[j].itemNum].infected = true;
                  coords[infectedAll[j].itemNum].color = 'red';
                }
                
                if(infectedAll.length && $scope.editor.vax<0){
                
                infectedNodes = [];
                finishGame = true;
                
                for(var i=0;i<coords.length;i++){
                  if(coords[i].infected)
                    infectedNodes.push(coords[i]);
                }
                

                for(var o=0;o<infectedNodes.length;o++){
                  for(var i=0;i<links.length;i++){
                    if((links[i].source.index == infectedNodes[o].index && !links[i].target.infected && !links[i].target.immun) || (links[i].target.index == infectedNodes[o].index && !links[i].source.infected && !links[i].source.immun)){
                      finishGame = false;
                      break;
                    } 
                  }
                }
              }
                
              if(finishGame && !finishKey){
                  finishKey = 1;
                var infectedNodes = 0;
                var uninfectedNodes = 0;
                for(var i=0;i<coords.length;i++){
                  if(coords[i].infected)
                    infectedNodes++;
                  else
                    uninfectedNodes++;
                }
                
                var percents = {
                  infected: infectedNodes/roundNum*100,
                  vaxed: vaxNum/roundNum*100,
                  caranted: $scope.editor.carant/roundNum*100,
                  uninfected: uninfectedNodes/roundNum*100,
                  diff: diffucilty,
                  me: nodePhotos.me
                };
                
                $scope.game.percentsUpdate(percents);
                $uibModal.open({
                          animation: true,
                          templateUrl: '../../views/includes/modalResultsWindow.html',
                          controller: 'ModalCtrl',
                          openedClass: 'modal-quarant',
                          controllerAs: 'modal',
                          size: 500,
                          backdrop: 'static',
                          resolve: {
                            items: function () {
                              return $scope.items;
                            }
                          }
                        });
                $scope.editor.finish = 'Congrats! You finish game!';
              }
                clickedOn = false;
                }, 600);} else{
                clickedOn = false;
              }
                  
                  if(!$scope.editor.vax && diffucilty==0)
                      $uibModal.open({
                          animation: true,
                          templateUrl: '../../views/includes/modalQuarantWindow.html',
                          controller: 'ModalCtrl',
                          openedClass: 'modal-quarant',
                          controllerAs: 'modal',
                          size: 500,
                          backdrop: 'static',
                          resolve: {
                            items: function () {
                              return $scope.items;
                            }
                          }
                        });

              
            }
            
            function gravity(alpha) {
                return function(d) {
                  var cy, cx;
                  if(d.y<=height/2)
                    cy = 0;
                  else
                    cy = height;
                  if(d.x<=width/2)
                    cx = 0;
                  else
                    cx = width;
                  d.y += (cy - d.y) * alpha;
                  d.x += (cx - d.x) * alpha;
                };
              }
              
              $scope.range = function(n){
                  return new Array(n);
              };
            
              function changeBackground(bg){
                if(bg=='infected-text'){
                  $('.d3-tip').addClass('infected-tip');
                  $('.d3-tip').removeClass('immun-tip');
                  $('.d3-tip').removeClass('refuser-tip');
                }
                else if(bg=='immun-text'){
                  $('.d3-tip').addClass('immun-tip');
                $('.d3-tip').removeClass('infected-tip');
                  $('.d3-tip').removeClass('refuser-tip');
                }
                else if(bg=='refuser-text'){
                  $('.d3-tip').addClass('refuser-tip');
                  $('.d3-tip').removeClass('infected-tip');
                  $('.d3-tip').removeClass('immun-tip');
                }
                else{
                  $('.d3-tip').removeClass('infected-tip');
                  $('.d3-tip').removeClass('immun-tip');
                  $('.d3-tip').removeClass('refuser-tip');
                }
              }
              
              firstRun = true;

          });
          });
        });
             });
        }
      };
    }]);