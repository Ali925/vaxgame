'use strict';

angular.module('vaxApp.directives').directive( 'bgParallax',
  ['d3Factory', '$q', '$window', '$interval', '$document','$timeout', '$rootScope',
    function (d3Factory, $q, $window, $interval, $document, $timeout, $rootScope) {
      
      return {
        scope: true,
        restrict: 'A',
        
        link: function($scope, $element, $attrs){
          d3Factory.d3().then(function(d3){
            
          var nodes = [
            {
              image: "./../assets/img/item8-2.png",
              width: 188,
              height: 141
            },
            {
              image: "./../assets/img/item2-2.png",
              width: 247,
              height: 220
            },
            {
              image: "./../assets/img/item3-2.png",
              width: 249,
              height: 272
            },
            {
              image: "./../assets/img/item6-2.png",
              width: 361,
              height: 360
            },
            {
              image: "./../assets/img/item1-1.png",
              width: 526,
              height: 600
            },
            {
              image: "./../assets/img/item4-1.png",
              width: 727,
              height: 727
            },
            {
              image: "./../assets/img/item7-1.png",
              width: 729,
              height: 705
            },
            {
              image: "./../assets/img/item5-1.png",
              width: 904,
              height: 806
            }
          ];
            
            var links = [];
          
            var height = window.innerHeight;
            var width = window.innerWidth;
            
            var svg = d3.select($element[0]).append('svg').attr("id", "svg-bg")
                              .attr('height', "100%");

            
            var g = svg.append('g');
        
            var underlay = g.append("rect")
                .attr("class", "bg-underlay")
                .attr("fill", "rgb(34,92,107)")
                .attr("width", "100%")
                .attr("height", "100%");
            
            var image = g.selectAll('image.bg').data(nodes).enter().append('image')
                        .attr('xlink:href', function(d, i){return d.image;})
                        .attr('width', function(d, i){return d.width;})
                        .attr('height', function(d, i){return d.height;}).attr('x', 0).attr('y', 0);

            var web = d3.layout.force();
            
              web.friction(.83)
                  .charge(-900) 
                  .gravity(0.01)
                  .size([width, height])
                  .on("tick", tick);
            
            var drag = web.drag();
            
            image.call(drag);
            
            web
              .nodes(nodes)
              .links(links)
              .start();
            
            g.append('rect').attr('x', 0).attr('y', 0)
                    .attr('width', '100%').attr('height', '100%')
                    .attr("fill", "rgba(0,0,0,.7)").style('pointer-events', 'none');
            
            function tick(){
              image.attr('x', function(d, i){
                return d.x = Math.max(-180, Math.min(width-180, d.x));
              })
                .attr('y', function(d, i){
                return d.y = Math.max(-180, Math.min(height-180, d.y));
              });
            }
          
      });
      } 
      }
}]);