'use strict';

angular.module('vaxApp.directives').directive( 'resultsServer',
  ['d3Factory', '$q', '$window', '$location', '$cookies','$timeout','$routeParams', '$http',
    function (d3Factory, $q, $window, $location, $cookies, $timeout, $routeParams, $http) {
      
      return {
        scope: true,
        restrict: 'A',
        
        link: function($scope, $element, $attrs){
          
          $scope.results = [{}];
          $scope.resultData = {};
          $scope.quantity = 10;
          
          var diff = $scope.game.getChoice();
         
          switch(diff){
            case '0':
              $scope.resultData.diff = 'result_p'; $scope.order = '-result_p'; break;
            case '1':
              $scope.resultData.diff = 'result_e'; $scope.order = '-result_e'; break;
            case '2':
              $scope.resultData.diff = 'result_n'; $scope.order = '-result_n'; break;
            case '3':
              $scope.resultData.diff = 'result_h'; $scope.order = '-result_h'; break;
          }
          
          $timeout(function(){
            $http({
            method: 'GET',
            url: '../../results.php'
          }).then(function successCallback(response){
              $scope.results = response.data;
              for(var i=0;i<response.data.length;i++){
                var mainResult = Math.round(parseInt(response.data[i].result_p)/10) + parseInt(response.data[i].result_e) + parseInt(response.data[i].result_n)*2 + parseInt(response.data[i].result_h)*4;
                $scope.results[i].mainResult = mainResult;
                $scope.results[i].result_p = parseInt($scope.results[i].result_p);
                $scope.results[i].result_e = parseInt($scope.results[i].result_e);
                $scope.results[i].results_n = parseInt($scope.results[i].results_n);
                $scope.results[i].result_h = parseInt($scope.results[i].results_h);
              }
          }, function errorCallback(response){});
          }, 1000);
          
          $scope.share = {
                    vk: function() {       
                       var url  = 'http://vkontakte.ru/share.php?url=http%3A//vaxgame.netgon.ru/';
                        this.popup(url);
                    },
                    fb: function() {
                       var url  = 'https://www.facebook.com/sharer/sharer.php?u=http%3A//vaxgame.netgon.ru/';
                        this.popup(url);
                    },
                    tw: function() {
                       var url  = 'https://twitter.com/intent/tweet?url=http%3A//vaxgame.netgon.ru/';
                        this.popup(url);
                    },
                    popup: function(url) {
                        window.open(url,'','toolbar=0,status=0,width=626,height=436');
                    }
                };
            
        }
      }
    }]);