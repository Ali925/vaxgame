'use strict';

angular.module('vaxApp.directives').directive( 'tutorialEditor',
  ['d3Factory', '$q', '$window',  '$document','$timeout','$rootScope', 'vkFactory', '$interval', '$uibModal', '$location',
    function (d3Factory, $q, $window, $document, $timeout, $rootScope, vkFactory, $interval, $uibModal, $location) {
      
      return {
        scope: true,
        restrict: 'A',
        
        link: function($scope, $element, $attrs){
          
          if($scope.tutorialPassed==='passed'){
            $location.path('/game');
            $location.replace();
          }
          else
          vkFactory.VK().then(function(VK){
            
            if($scope.tutorialPassedAdd==='passed')
              $scope.tutorialPassedChange();
              
              var firstRun;
            var nodePhotos = {};
            nodePhotos.me = {};
            nodePhotos.friends = [];        
            nodePhotos.friendsFriends = [];
            var friendsNum = 0;
            var ids = [];
            var friendsFriends = [];
            var friendsArray = [];
              
            
            $scope.vkDataLoaded = undefined;
 
            
            if($scope.game.vkAuthed)
              VK.Auth.getLoginStatus(function authInfo(response) {
                        if (response.session) {
                          ids.push(parseInt(response.session.mid));
                          VK.api('friends.get', {user_id: response.session.mid}, function(data){
                            if(data.response && data.response.length){
                              friendsArray = data.response;
                              if(friendsArray.length>5){
                                          for(var i=0;i<6;i++)
                                              ids.push(friendsArray[i]);
                                            friendsNum = 6;
                                          
                                        } else if(friendsArray.length<=5){
                                          for(var i=0;i<friendsArray.length;i++){
                                              ids.push(friendsArray[i]);
 
                                          }
                                          friendsNum = friendsArray.length;
                                        } 
                              for(var j=0, t=0;j<friendsArray.length;j++){
                                if(j==2)
                                  break;
                                else
                              VK.api('friends.get', {user_id: friendsArray[j]}, function(d){
       
                                if(d.response && d.response.length){
                                  
                                  friendsFriends.push(d.response);
                                  
                                  if(t==1 || friendsArray.length<2){
                                        if(friendsFriends[0].length>5){
                                          var m = 0;
                                          friendsArray.filter(function(n){
                                            var index = friendsFriends[0].indexOf(n);
                                              if(ids.indexOf(n)==-1 && m<2 && index!=-1){
                                                m++;
                                                if(ids.length>=7)
                                                  ids[4+m] = friendsFriends[0][index];
                                                else {
                                                  ids.push(friendsFriends[0][index]);
                                                  friendsNum++;
                                                }
                                              }
                                                 
                                          });
                                          var idsNum = ids.length;
                                          if(friendsFriends.length>1)
                                          friendsFriends[0].filter(function(n){
                                              var index = friendsFriends[1].indexOf(n);
                                              if(ids.indexOf(n)==-1 && ids.length<=idsNum && index!=-1)
                                                 ids.push(friendsFriends[1][index]);
                                          });
                                          if(idsNum<ids.length)
                                          for(var i=0;i<2;i++){
                                            if(ids.indexOf(friendsFriends[0][i])==-1 && friendsFriends[0][i]!=ids[idsNum])
                                              ids.push(friendsFriends[0][i]);
                                            else
                                              i--;
                                          }
                                            else
                                              for(var i=0;i<3;i++){
                                            if(ids.indexOf(friendsFriends[0][i])==-1)
                                              ids.push(friendsFriends[0][i]);
                                            else
                                              i--;
                                          }
                                        } else if(friendsFriends[0].length<=3){
                                          var idsNum = ids.length;
                                          if(friendsFriends.length>1)
                                          friendsFriends[0].filter(function(n){
                                              var index = friendsFriends[1].indexOf(n);
                                              if(ids.indexOf(n)==-1 && ids.length<=idsNum && index!=-1)
                                                 ids.push(friendsFriends[1][index]);
                                          });
                                          if(idsNum<ids.length)
                                          for(var i=0;i<friendsFriends[0].length-1;i++){
                                            if(ids.indexOf(friendsFriends[0][i])==-1 && friendsFriends[0][i]!=ids[idsNum])
                                              ids.push(friendsFriends[0][i]);
                                          }
                                            else
                                              for(var i=0;i<friendsFriends[0].length;i++){
                                            if(ids.indexOf(friendsFriends[0][i])==-1)
                                              ids.push(friendsFriends[0][i]);
                                          }
                                        }
                                    
                                    console.log(ids);
                                     VK.api('users.get', {uids: ""+ids.join()+"", fields: 'photo'}, function(photos){
                            nodePhotos.me = photos.response[0];
                            for(var i=1;i<=friendsNum;i++){
                              nodePhotos.friends.push(photos.response[i]);
                            }           
                            for(var i=friendsNum+1;i<photos.response.length;i++){
                              nodePhotos.friendsFriends.push(photos.response[i]);
                            }           
                            $scope.vkDataLoaded = 'loaded';
                            $scope.$apply();
                            console.log(nodePhotos);
                  });
                                }

                            }
                                t++;
                              });
                            }
                            }
                            else{
                              VK.api('users.get', {uids: ""+ids.join()+"", fields: 'photo'}, function(photos){
                            nodePhotos.me = photos.response[0];    
                            $scope.vkDataLoaded = 'loaded';
                            $scope.$apply();
                  });
                            }
                          });
                          
                        } else {
                          alert('not auth');
                        }
                      });
            else
              $scope.vkDataLoaded = 'noloaded';
          
        $scope.$watch('vkDataLoaded', function(newVar, oldVar){
          if(newVar!==undefined && firstRun===undefined)
          d3Factory.d3().then(function(d3){
            
            var height = $('.tutorial-editor').height();
            var width = $('.tutorial-editor').width();
            
            var tutorialHeight = height;  
              
            var topInfo = $(window).height()*.26;
            var nextButton = $(window).height()*.33;
            
            $('.tutorial-info').css('top', height+topInfo);
            $('.prev-next-buttons').css('top', height+nextButton);
            $('.skip-button').css('top', height+nextButton);
            
            $scope.editor = {};
            
            $scope.editor.infoNum = 1;
            $scope.editor.infoAnsw = 0;
            $scope.editor.vax = 2;
            $scope.editor.carant = 0;
            $scope.editor.tutorialFinished = false
            $scope.editor.tutorialResult = 0;
            
            $scope.editor.svg = {};
            $scope.editor.svg.rootNode = d3.select($element[0]).append('svg').attr("id", "svg-editor")
                                            .attr("width", width)
                                            .attr("height", height);
                            
           
            var tip = d3.tip()
                  .attr('class', 'd3-tip')
                  .html(function(d){
                    if(d!==undefined && d.infected){
                      $timeout(function(){changeBackground('infected-text');});
                      return "<p class='infected-text'>"+d.name.first_name + " " + d.name.last_name+"</p><p>инфицирован</p>";
                    }
                    else if(d!==undefined && d.carantine){
                      $timeout(function(){changeBackground('carant-text');});
                      return "<p class='carant-text'>"+d.name.first_name + " " + d.name.last_name+"</p><p>на карантине</p>";
                    }
                    else if(d!==undefined && d.vaxed){
                      $timeout(function(){changeBackground('vaxed-text');});
                      return "<p class='vaxed-text'>"+d.name.first_name + " " + d.name.last_name+"</p><p>вакцинирован</p>";
                    }
                    else{
                      $timeout(function(){changeBackground('d3tip-text');});
                      return "<p>"+d.name.first_name + " " + d.name.last_name+"</p>";
                    }
                  })
                  .direction('n')
                  .offset([-10, 0]);
            
            var g = $scope.editor.svg.rootNode
                .append("g");
            
            g.call(tip);

            $scope.editor.svg.underlay = g.append("rect")
                .attr("class", "underlay")
                .attr("width", "100%")
                .attr("height", "100%");
            
            var coords = [], links = [], circles, radiusBigger, photos, clipPath, clipPathCircles, svgLinks, infectionsLoaded = true, dragged=false, dragTime, caranted = 0, photosBg, isMouseOver = false;
            
            start(false);
              
             var web = d3.layout.force();
            
              web.friction(.83)
                  .charge(-900) 
                  .size([width, height])
                  .on("tick", tick);
            
              
           var drag = web.drag()
           .on("dragstart", function(){
              dragTime = new Date(); 
              dragged=true;
            })
            .on("dragend", function(){
              var finishTime = new Date();
              if((finishTime-dragTime)<250)
                dragged = false;
            });
            
            circles.call(drag);
            if($scope.vkDataLoaded=="loaded")
              photos.call(drag);
            
            web.nodes(coords)
                .links(links)
                .start();
            
             function tick(e){
              circles.each(gravity(0.01 * e.alpha))
                    .attr("cx", function(d){
                        return d.x = Math.max((d.r+3), Math.min(width - d.r - 3, d.x));
                    })
                      .attr("cy", function(d){
                     return d.y = Math.max((d.r+3), Math.min(height - d.r - 3, d.y));
                    });
               
               if($scope.vkDataLoaded=="loaded"){
               photos.each(gravity(0.01 * e.alpha))
                    .attr('clip-path', function(d, i){ return "url(#clip" + d.index + ")"; })
                    .attr("x", function(d){
                        return (d.x-d.r);
                    })
                      .attr("y", function(d){
                     return (d.y-d.r);
                    })
                      .attr("width", function(d){
                        return (d.r*2);
                      })
                      .attr("height", function(d){
                        return (d.r*2);
                      });
              clipPath.attr('id', function(d) { return "clip" + d.index; });
              clipPathCircles.each(gravity(0.01 * e.alpha))
                    .attr("cx", function(d){
                        return d.x = Math.max((d.r+3), Math.min(width - d.r - 3, d.x));
                    })
                      .attr("cy", function(d){
                     return d.y = Math.max((d.r+3), Math.min(height - d.r - 3, d.y));
                    })
                      .attr('r', function(d){return (d.r);});
              }
              
              svgLinks.attr('x1', function(d){return d.source.x;})
                      .attr('y1', function(d){return d.source.y;})
                      .attr('x2', function(d){return d.target.x;})
                      .attr('y2', function(d){return d.target.y;});
            }
            
            function gravity(alpha) {
                return function(d) {
                  var cy, cx;
                  if(d.y<=height/2)
                    cy = 0;
                  else
                    cy = height;
                  if(d.x<=width/2)
                    cx = 0;
                  else
                    cx = width;
                  d.y += (cy - d.y) * alpha;
                  d.x += (cx - d.x) * alpha;
                };
              }
            
            $scope.editor.nextStep = function(){
              if($scope.editor.infoNum<6 && infectionsLoaded){
                if($scope.editor.infoNum==1){   
                  $scope.editor.infoNum++;
                  $('.tutorial-editor').css('margin-top', '-12%');   
                    
                 $('.tutorial-info').css('top', tutorialHeight+topInfo+30);   
                  coords[1].stroke = "red";
                  update();
                }
                else if($scope.editor.infoNum==2){
                    
                  if($scope.editor.infoAnsw==1){
                    $scope.editor.infoNum++;
                      $('.tutorial-editor').css('margin-top', '-10%');
                      $('.tutorial-info').css('top', tutorialHeight+topInfo+15);
                    coords[1].stroke = "green";
                    coords[1].vaxed = true;
                    coords[0].color = "red";
                    coords[0].stroke= "red";
                    coords[0].infected = true;
                    for(var i=0;i<links.length;i++){
                      if(links[i].source.index == coords[1].index || links[i].target.index == coords[1].index){
                        links.splice(i, 1);
                        i--;
                      }
                    }
                    remove(true);
                  } else if($scope.editor.infoAnsw==2){
                      $('.tutorial-editor').css('margin-top', '-7%');
                      $('.tutorial-info').css('top', tutorialHeight+topInfo+5);
                    $scope.editor.infoNum++;
                    coords[1].color = "red";
                    coords[1].infected = true;
                    update();
                  }
                }
                else if($scope.editor.infoNum==3){
                    $('.tutorial-editor').css('margin-top', '-10%');
                  $('.tutorial-info').css('top', tutorialHeight+topInfo+30);
                  if($scope.editor.infoAnsw==1){
                    infectionsLoaded = false;
                    $scope.editor.infoNum++;
                    var infectedAll = [];

                      for(var i=0;i<links.length;i++){
                        if(links[i].source.index == coords[0].index){
                          infectedAll.push(links[i].target);
                          coords[links[i].target.index].color = "red";
                          coords[links[i].target.index].stroke = "red";
                          coords[links[i].target.index].infected = true;
                        }
                        else if(links[i].target.index == coords[0].index){
                          infectedAll.push(links[i].source);
                          coords[links[i].source.index].color = "red";
                          coords[links[i].source.index].stroke = "red";
                          coords[links[i].source.index].infected = true;
                        }
                      }
                      
                      g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                      .attr("transform", function(d, i){
                          return "translate("+coords[0].x+", "+coords[0].y+")";
                        })
                      .attr("class", "infection");
                    g.selectAll("circle.infection").transition()
                          .duration(500)
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].x+", "+infectedAll[i].y+")";
                      })
                      .remove();
                      $timeout(function(){update();},550);
                      
                      infectedAll = [];
                    $timeout(function(){
                      infectedAll = [];
                    for(var i=0;i<coords.length;i++){
                      if(!coords[i].infected && i!=1){
                        coords[i].color = "red";
                        coords[i].stroke = "red";
                        coords[i].infected = true;  
                        for(var j=0;j<links.length;j++){
                          if(links[j].source.index == coords[i].index && links[j].target.infected)
                            infectedAll.push({
                              source: links[j].target,
                              target: coords[i]
                            });
                          else if(links[j].target.index == coords[i].index && links[j].source.infected)
                            infectedAll.push({
                              source: links[j].source,
                              target: coords[i]
                            });
                        }
                        
                      }
                    }
                    
                     g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].source.x+", "+infectedAll[i].source.y+")";
                        })
                      .attr("class", "infection");
                    g.selectAll("circle.infection").transition()
                          .duration(500)
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].target.x+", "+infectedAll[i].target.y+")";
                      })
                      .remove();
                      $timeout(function(){update();infectionsLoaded = true;},550);
                                          
                    }, 1300);
                    
                    
                  } else if($scope.editor.infoAnsw==2){
                    infectionsLoaded = false;
                    $scope.editor.infoNum++;
                    var infectedAll = [];

                      for(var i=0;i<links.length;i++){
                        if(links[i].source.index == coords[1].index){
                          infectedAll.push(links[i].target);
                          coords[links[i].target.index].color = "red";
                          coords[links[i].target.index].stroke = "red";
                          coords[links[i].target.index].infected = true;
                        }
                        else if(links[i].target.index == coords[1].index){
                          infectedAll.push(links[i].source);
                          coords[links[i].source.index].color = "red";
                          coords[links[i].source.index].stroke = "red";
                          coords[links[i].source.index].infected = true;
                        }
                      }
                      
                      g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                      .attr("transform", function(d, i){
                          return "translate("+coords[1].x+", "+coords[1].y+")";
                        })
                      .attr("class", "infection");
                    g.selectAll("circle.infection").transition()
                          .duration(500)
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].x+", "+infectedAll[i].y+")";
                      })
                      .remove();
                      $timeout(function(){update();},550);
                      
                      infectedAll = [];
                    $timeout(function(){
                      infectedAll = [];
                    for(var i=0;i<coords.length;i++){
                      if(!coords[i].infected){
                        coords[i].color = "red";
                        coords[i].stroke = "red";
                        coords[i].infected = true;  
                        for(var j=0;j<links.length;j++){
                          if(links[j].source.index == coords[i].index && links[j].target.infected)
                            infectedAll.push({
                              source: links[j].target,
                              target: coords[i]
                            });
                          else if(links[j].target.index == coords[i].index && links[j].source.infected)
                            infectedAll.push({
                              source: links[j].source,
                              target: coords[i]
                            });
                        }
                        
                      }
                    }
                    
                     g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].source.x+", "+infectedAll[i].source.y+")";
                        })
                      .attr("class", "infection");
                    g.selectAll("circle.infection").transition()
                          .duration(500)
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].target.x+", "+infectedAll[i].target.y+")";
                      })
                      .remove();
                      $timeout(function(){update();infectionsLoaded = true;},550);
                                          
                    }, 1300);
                    
                    
                  }
                } else if($scope.editor.infoNum == 4){
                  $('.tutorial-editor').css('margin-top', '-13%');
                  $('.tutorial-info').css('top', tutorialHeight+topInfo+35);
                  $scope.editor.infoNum++;
                  $scope.editor.vax = 2;
                  coords = [];
                  links = [];
                  remove(true);
                  start(true);
                  
                  if($scope.vkDataLoaded=="loaded")
                    photos.on("click", function(d, t){
                    if(!dragged && !coords[d.index].infected && !coords[d.index].vaxed && !coords[d.index].carantine){
                      tip.hide(d);
                      $scope.editor.vax--;
                      $scope.$apply();
                      if($scope.editor.vax<0)
                        caranted++;
                    var i = 0, removedLines = [], n=0,m;
                    while(i < links.length) {
                      if ((links[i].source.index == d.index) || (links[i].target.index == d.index)) {
                        removedLines.push(i);
                      }
                      i++;
                   }
                    if($scope.editor.vax>-1){
                      coords[d.index].stroke = 'green';
                      coords[d.index].vaxed = true;
                    }
                    else{
                      coords[d.index].stroke = 'blue';
                      coords[d.index].carantine = true;
                    }
                      update();
                    for(var i=0;i<removedLines.length;i++){
                      m=removedLines[i];
                      if(n>0)
                        m=m-n;
                      links.splice(m, 1);
                      n++;
                    }
                    remove(false);
                      if(!$scope.editor.vax){ 
                          $('.tutorial-editor').css('margin-top', '-11.5%');
                          $('.tutorial-info').css('top', tutorialHeight+topInfo+40);
                        var found = true;
                        var randomNode;
                        while(found){
                          randomNode = Math.floor(Math.random()*coords.length);
                          for(var i=0;i<links.length;i++){
                            if(coords[randomNode].index == links[i].source.index || coords[randomNode].index == links[i].target.index){
                             found=false;
                              break;
                            }       
                          }
                        }
                        coords[randomNode].color = "red";
                        coords[randomNode].stroke = "red";
                        coords[randomNode].infected = true;
                        update();
                      } else if($scope.editor.vax<0){
                        var infectedNodes = [], infectedLink, found = true, randomNode;
                        for(var i=0;i<coords.length;i++){
                          if(coords[i].infected)
                            infectedNodes.push(coords[i]);
                        }
                        while(found){
                          randomNode = Math.floor(Math.random()*infectedNodes.length);
                          for(var i=0;i<links.length;i++){
                            if(infectedNodes[randomNode].index == links[i].source.index && !links[i].target.infected){
                              infectedLink = links[i].target;
                              found = false;
                              break;
                            } else if(infectedNodes[randomNode].index == links[i].target.index && !links[i].source.infected){
                              infectedLink = links[i].source;
                              found = false;
                              break;
                            }
                              
                          }
                          if(found && infectedNodes.length){
                            infectedNodes.splice(randomNode, 1);
                          } 
                          if(!infectedNodes.length)
                            found = false;
                        }
                           
                        if(infectedNodes.length){
                          for(var i=0;i<coords.length;i++){
                            if(coords[i].index == infectedLink.index){
                              coords[i].color = "red";
                              coords[i].stroke = "red";
                              coords[i].infected = true;
                            }
                          }
                          g.selectAll(".infection").data([1]).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                            .attr("transform", function(d, i){
                                return "translate("+infectedNodes[randomNode].x+", "+infectedNodes[randomNode].y+")";
                              })
                            .attr("class", "infection");
                          g.selectAll("circle.infection").transition()
                                .duration(500)
                            .attr("transform", function(d, i){
                                return "translate("+infectedLink.x+", "+infectedLink.y+")";
                            })
                            .remove();
                            $timeout(function(){update();},550);
                        } 
                        infectedNodes = [], found=true;
                        for(var i=0;i<coords.length;i++){
                          if(coords[i].infected)
                            infectedNodes.push(coords[i]);
                        }
                        while(found){
                          randomNode = Math.floor(Math.random()*infectedNodes.length);
                          for(var i=0;i<links.length;i++){
                            if(infectedNodes[randomNode].index == links[i].source.index && !links[i].target.infected){
                              found = false;
                              break;
                            } else if(infectedNodes[randomNode].index == links[i].target.index && !links[i].source.infected){
                              found = false;
                              break;
                            }
                              
                          }
                          if(found && infectedNodes.length){
                            infectedNodes.splice(randomNode, 1);
                          } 
                          if(!infectedNodes.length)
                            found = false;
                        }
                        if(!infectedNodes.length)
                          $timeout(function(){
                            $scope.editor.tutorialFinished = true;
                              $uibModal.open({
                                  animation: true,
                                  templateUrl: '../../views/includes/modalTutorialWindow.html',
                                  controller: 'ModalCtrl',
                                  controllerAs: 'modal',
                                  openedClass: 'modal-quarant modal-tutorial',
                                  size: 500,
                                  backdrop: 'static',
                                  resolve: {
                                    items: function () {
                                      return $scope.items;
                                    }
                                  }
                                });
                          },600);
                          
                      }
                    }
                    dragged=false;
                  });
                  
                  circles.on("click", function(d, t){
                    if(!dragged && !coords[d.index].infected && !coords[d.index].vaxed && !coords[d.index].carantine){
                      tip.hide(d);
                      $scope.editor.vax--;
                      $scope.$apply();
                      if($scope.editor.vax<0)
                        caranted++;
                    var i = 0, removedLines = [], n=0,m;
                    while(i < links.length) {
                      if ((links[i].source.index == d.index) || (links[i].target.index == d.index)) {
                        removedLines.push(i);
                      }
                      i++;
                   }
                    if($scope.editor.vax>-1){
                      coords[d.index].stroke = 'green';
                      coords[d.index].vaxed = true;
                    }
                    else{
                      coords[d.index].stroke = 'blue';
                      coords[d.index].carantine = true;
                    }
                      update();
                    for(var i=0;i<removedLines.length;i++){
                      m=removedLines[i];
                      if(n>0)
                        m=m-n;
                      links.splice(m, 1);
                      n++;
                    }
                    remove(false);
                      if(!$scope.editor.vax){ 
                          $('.tutorial-editor').css('margin-top', '-11.5%');
                          $('.tutorial-info').css('top', tutorialHeight+topInfo+40);
                        var found = true;
                        var randomNode;
                        while(found){
                          randomNode = Math.floor(Math.random()*coords.length);
                          for(var i=0;i<links.length;i++){
                            if(coords[randomNode].index == links[i].source.index || coords[randomNode].index == links[i].target.index){
                             found=false;
                              break;
                            }       
                          }
                        }
                        coords[randomNode].color = "red";
                        coords[randomNode].stroke = "red";
                        coords[randomNode].infected = true;
                        update();
                      } else if($scope.editor.vax<0){
                        var infectedNodes = [], infectedLink, found = true, randomNode;
                        for(var i=0;i<coords.length;i++){
                          if(coords[i].infected)
                            infectedNodes.push(coords[i]);
                        }
                        while(found){
                          randomNode = Math.floor(Math.random()*infectedNodes.length);
                          for(var i=0;i<links.length;i++){
                            if(infectedNodes[randomNode].index == links[i].source.index && !links[i].target.infected){
                              infectedLink = links[i].target;
                              found = false;
                              break;
                            } else if(infectedNodes[randomNode].index == links[i].target.index && !links[i].source.infected){
                              infectedLink = links[i].source;
                              found = false;
                              break;
                            }
                              
                          }
                          if(found && infectedNodes.length){
                            infectedNodes.splice(randomNode, 1);
                          } 
                          if(!infectedNodes.length)
                            found = false;
                        }
                           
                        if(infectedNodes.length){
                          for(var i=0;i<coords.length;i++){
                            if(coords[i].index == infectedLink.index){
                              coords[i].color = "red";
                              coords[i].stroke = "red";
                              coords[i].infected = true;
                            }
                          }
                          g.selectAll(".infection").data([1]).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                            .attr("transform", function(d, i){
                                return "translate("+infectedNodes[randomNode].x+", "+infectedNodes[randomNode].y+")";
                              })
                            .attr("class", "infection");
                          g.selectAll("circle.infection").transition()
                                .duration(500)
                            .attr("transform", function(d, i){
                                return "translate("+infectedLink.x+", "+infectedLink.y+")";
                            })
                            .remove();
                            $timeout(function(){update();},550);
                        } 
                        infectedNodes = [], found=true;
                        for(var i=0;i<coords.length;i++){
                          if(coords[i].infected)
                            infectedNodes.push(coords[i]);
                        }
                        while(found){
                          randomNode = Math.floor(Math.random()*infectedNodes.length);
                          for(var i=0;i<links.length;i++){
                            if(infectedNodes[randomNode].index == links[i].source.index && !links[i].target.infected){
                              found = false;
                              break;
                            } else if(infectedNodes[randomNode].index == links[i].target.index && !links[i].source.infected){
                              found = false;
                              break;
                            }
                              
                          }
                          if(found && infectedNodes.length){
                            infectedNodes.splice(randomNode, 1);
                          } 
                          if(!infectedNodes.length)
                            found = false;
                        }
                        if(!infectedNodes.length)
                          $timeout(function(){
                            $scope.editor.tutorialFinished = true;
                              $uibModal.open({
                                  animation: true,
                                  templateUrl: '../../views/includes/modalTutorialWindow.html',
                                  controller: 'ModalCtrl',
                                  controllerAs: 'modal',
                                  openedClass: 'modal-quarant modal-tutorial',
                                  size: 500,
                                  backdrop: 'static',
                                  resolve: {
                                    items: function () {
                                      return $scope.items;
                                    }
                                  }
                                });
                          },600);
                          
                      }
                    }
                    dragged=false;
                  });
                } else if($scope.editor.infoNum==5 && $scope.editor.tutorialFinished){
                  $scope.editor.infoNum++;
                  var inf = 0, uninf = 0;
                  for(var i=0;i<coords.length;i++){
                    if(coords[i].infected)
                      inf++;
                  }
                  
                  var percents = {
                    infected: inf*10,
                    vaxed: 2*10,
                    caranted: caranted*10,
                    uninfected: (10-inf-2-caranted)*10
                  };
                  
                  
                  $scope.editor.tutorialResult = 100-Math.round(percents.infected);
                  
                  coords = [];
                  links = [];
                  remove();
                  
                  var x = 10, y = 20, cy = 25, text, py = 29;
                 
                 $('.tutorial-editor').css('margin-top', '-8%');
                 $("#svg-editor").css('margin-top', '100px');
                var containerHeight = $('#svg-editor').height()*.6;
                var containerWidth =   $('#svg-editor').width()*.4;  
                  
              var heightMax = containerHeight/100;
              
            for(var i=0;i<4;i++){
              var color, height;
              if(!i){
                x=containerWidth+.1;
                color = 'rgb(239,85,85)';
                height = percents.infected*heightMax;
                text = 'Инфицированы';
              }
              else if(i===1){
                x = containerWidth;
                color = 'rgb(65,171,201)';
                height = percents.vaxed*heightMax;
                text = 'Вакцинированы';
              }
              else if(i===2){
                x = containerWidth;
                color = 'rgb(47,127,36)';
                height = percents.caranted*heightMax;
                text = 'На карантине';
              }
              else if(i===3){
                x = containerWidth;
                color = 'rgb(36,89,128)';
                height = percents.uninfected*heightMax;
                text = 'Не инфицированы';
              }
              coords.push({
                x: x,
                y: y,
                height: height,
                color: color,
                cx: x+55,
                cy: cy,
                px: x+74,
                py: py,
                text: text
              });
              y=height+y;
              cy += Math.round(15.126*heightMax);
              py += Math.round(15.126*heightMax);
            }  
              
              g.selectAll("rect.detailed").data(coords).enter().append('rect')
                  .attr('x', function(d){return d.x;}).attr('y', function(d){return d.y;})
                  .attr('height', function(d){return d.height;}).attr('width', 35)
                  .attr('fill', function(d){return d.color;}).attr('class', 'detailed');
              
              g.selectAll('circle.title-circle').data(coords).enter().append('circle')
                .attr('cx', function(d){return d.cx;}).attr('cy', function(d){return d.cy;})
                .attr('r', 5).attr('fill', function(d){return d.color;});
              
              g.selectAll('text.title-detail').data(coords).enter().append('text')
                .attr('x', function(d){return d.px}).attr('y', function(d){return d.py})
                .text(function(d){return d.text;}).attr('fill', 'white').attr('class', 'title-detail');
                  
                  var heightInner = $('#svg-editor').height();
                  
                  $('.tutorial-result-text').css('top', heightInner+100);
                  
                }
              }
            };
            
            $scope.editor.prevStep = function(){
              if($scope.editor.infoNum>1 && infectionsLoaded){
                if($scope.editor.infoNum==2){
                  $scope.editor.infoNum--;
                    $('.tutorial-editor').css('margin-top', '-8%');
                    $('.tutorial-info').css('top', tutorialHeight+topInfo);
                  coords[1].stroke = "white";
                  update();
                }
                else if($scope.editor.infoNum==3){
                  coords = [];
                  links = [];
                  remove(true);
                  $scope.editor.infoNum--;
                    $('.tutorial-editor').css('margin-top', '-12%');
                    $('.tutorial-info').css('top', tutorialHeight+topInfo+30);
                  $scope.editor.infoAnsw = 0;
                  start(true);
                  coords[1].stroke = "red";
                  update();
                }
                else if($scope.editor.infoNum==4){
                  if($scope.editor.infoAnsw==1){
                    $('.tutorial-info').css('top', tutorialHeight+topInfo+15);
                    $scope.editor.infoNum--;
                    for(var i=0;i<coords.length;i++){
                      coords[i].color = "white";
                      coords[i].stroke = "white";
                      coords[i].infected = false;
                    }
                      coords[0].color = "red";
                      coords[0].stroke = "red";
                      coords[0].infected = true;
                      coords[1].stroke = "green";
                      coords[1].vaxed = true;
                      update();
                  }
                  else if($scope.editor.infoAnsw==2){
                    $scope.editor.infoNum--;
                      $('.tutorial-editor').css('margin-top', '-7%');
                    $('.tutorial-info').css('top', tutorialHeight+topInfo+5);
                    for(var i=0;i<coords.length;i++){
                      coords[i].color = "white";
                      coords[i].stroke = "white";
                      coords[i].infected = false;
                    }
                      coords[1].color = "red";
                      coords[1].stroke = "red";
                      coords[1].infected = true;
                      update();
                  }
                }
                else if($scope.editor.infoNum==5){
                    $('.tutorial-editor').css('margin-top', '-10%');
                    $('.tutorial-info').css('top', tutorialHeight+topInfo+30);
                  if($scope.editor.infoAnsw==1){
                    $scope.editor.infoNum--;
                      infectionsLoaded = false;
                    coords = [];
                    links = [];
                    remove(true);
                    start(true);
                    coords[1].stroke = "green";
                    coords[1].vaxed = true;
                    coords[0].color = "red";
                    coords[0].stroke= "red";
                    coords[0].infected = true;
                    for(var i=0;i<links.length;i++){
                      if(links[i].source.index == coords[1].index || links[i].target.index == coords[1].index){
                        links.splice(i, 1);
                        i--;
                      }
                    }
                    remove(true);
                    $timeout(function(){
                    var infectedAll = [];

                      for(var i=0;i<links.length;i++){
                        if(links[i].source.index == coords[0].index){
                          infectedAll.push(links[i].target);
                          coords[links[i].target.index].color = "red";
                          coords[links[i].target.index].stroke = "red";
                          coords[links[i].target.index].infected = true;
                        }
                        else if(links[i].target.index == coords[0].index){
                          infectedAll.push(links[i].source);
                          coords[links[i].source.index].color = "red";
                          coords[links[i].source.index].stroke = "red";
                          coords[links[i].source.index].infected = true;
                        }
                      }
                      
                      g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                      .attr("transform", function(d, i){
                          return "translate("+coords[0].x+", "+coords[0].y+")";
                        })
                      .attr("class", "infection");
                    g.selectAll("circle.infection").transition()
                          .duration(500)
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].x+", "+infectedAll[i].y+")";
                      })
                      .remove();
                      $timeout(function(){update();},550);
                      
                      infectedAll = [];
                    $timeout(function(){
                      infectedAll = [];
                    for(var i=0;i<coords.length;i++){
                      if(!coords[i].infected && i!=1){
                        coords[i].color = "red";
                        coords[i].stroke = "red";
                        coords[i].infected = true;  
                        for(var j=0;j<links.length;j++){
                          if(links[j].source.index == coords[i].index && links[j].target.infected)
                            infectedAll.push({
                              source: links[j].target,
                              target: coords[i]
                            });
                          else if(links[j].target.index == coords[i].index && links[j].source.infected)
                            infectedAll.push({
                              source: links[j].source,
                              target: coords[i]
                            });
                        }
                        
                      }
                    }
                    
                     g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].source.x+", "+infectedAll[i].source.y+")";
                        })
                      .attr("class", "infection");
                    g.selectAll("circle.infection").transition()
                          .duration(500)
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].target.x+", "+infectedAll[i].target.y+")";
                      })
                      .remove();
                      $timeout(function(){update();infectionsLoaded = true;},550);
                                          
                    }, 1300);
                    }, 800);
                  } else if($scope.editor.infoAnsw==2){
                    $scope.editor.infoNum--;
                      infectionsLoaded = false;
                    coords = [];
                    links = [];
                    remove(true);
                    start(true);
                    coords[1].color = "red";
                    coords[1].stroke= "red";
                    coords[1].infected = true;
                    update();
                    
                    $timeout(function(){
                      
                    var infectedAll = [];

                      for(var i=0;i<links.length;i++){
                        if(links[i].source.index == coords[1].index){
                          infectedAll.push(links[i].target);
                          coords[links[i].target.index].color = "red";
                          coords[links[i].target.index].stroke = "red";
                          coords[links[i].target.index].infected = true;
                        }
                        else if(links[i].target.index == coords[1].index){
                          infectedAll.push(links[i].source);
                          coords[links[i].source.index].color = "red";
                          coords[links[i].source.index].stroke = "red";
                          coords[links[i].source.index].infected = true;
                        }
                      }
                      
                      g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                      .attr("transform", function(d, i){
                          return "translate("+coords[1].x+", "+coords[1].y+")";
                        })
                      .attr("class", "infection");
                    g.selectAll("circle.infection").transition()
                          .duration(500)
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].x+", "+infectedAll[i].y+")";
                      })
                      .remove();
                      $timeout(function(){update();},550);
                      
                      infectedAll = [];
                    $timeout(function(){
                      infectedAll = [];
                    for(var i=0;i<coords.length;i++){
                      if(!coords[i].infected){
                        coords[i].color = "red";
                        coords[i].stroke = "red";
                        coords[i].infected = true;  
                        for(var j=0;j<links.length;j++){
                          if(links[j].source.index == coords[i].index && links[j].target.infected)
                            infectedAll.push({
                              source: links[j].target,
                              target: coords[i]
                            });
                          else if(links[j].target.index == coords[i].index && links[j].source.infected)
                            infectedAll.push({
                              source: links[j].source,
                              target: coords[i]
                            });
                        }
                        
                      }
                    }
                    
                     g.selectAll(".infection").data(infectedAll).enter().append("circle")
                                .attr("r", 5).attr("stroke", "white")
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].source.x+", "+infectedAll[i].source.y+")";
                        })
                      .attr("class", "infection");
                    g.selectAll("circle.infection").transition()
                          .duration(500)
                      .attr("transform", function(d, i){
                          return "translate("+infectedAll[i].target.x+", "+infectedAll[i].target.y+")";
                      })
                      .remove();
                      $timeout(function(){update();infectionsLoaded = true;},550);
                                          
                    }, 1300);
                    }, 800);
                  }
                }
              }
            };
            
            function start(isFirst){
              for(var i=0, radius;i<10;i++){
              if(!i)
                radius=17;
              else if(i==1)
                radius = 19;
              else
                radius = 12;
              coords.push({
                x: Math.floor(Math.random()*width)+1,
                y: Math.floor(Math.random()*height)+1,
                r: radius,
                color: "white",
                stroke: "white",
                infected: false,
                vaxed: false,
                carantine: false
              });
              
              if(!i){
                for(var j=1;j<6;j++)
                links.push({
                  source:i,
                  target:j
                });
                if(nodePhotos.friends.length){
                  coords[i].name = nodePhotos.friends[0];
                }
              } else if(i==1){
                for(var j=6;j<10;j++)
                  links.push({
                  source:i,
                  target:j
                });
                if(nodePhotos.me.photo){
                  coords[i].name = {first_name: "Вы", last_name: "", photo: nodePhotos.me.photo};
                }
              } else if(i>1 && i!=5 && i<8){
                links.push({
                  source:i,
                  target:i+1
                });
              } 
              if(i==2){
                links.push({
                  source:i,
                  target:1
                });
                if(nodePhotos.friends.length>=5){
                  coords[i].name = nodePhotos.friends[4];
                }
              }
              if(i==9){
                links.push({
                  source:i,
                  target:4
                });
                if(nodePhotos.friends.length>=2){
                  coords[i].name = nodePhotos.friends[1];
                }
              }
              if(i==7){
                links.push({
                  source:i,
                  target:0
                });
                if(nodePhotos.friends.length==6){
                  coords[i].name = nodePhotos.friends[5];
                }
              }
              if(i==3 && nodePhotos.friendsFriends.length>=2){
                coords[i].name = nodePhotos.friendsFriends[1];
              }
                
              if(i==4 && nodePhotos.friendsFriends.length){
                coords[i].name = nodePhotos.friendsFriends[0];
              }
              if(i==5 && nodePhotos.friendsFriends.length==3){
                coords[i].name = nodePhotos.friendsFriends[2];
              }
              if(i==6 && nodePhotos.friends.length>=3){
                coords[i].name = nodePhotos.friends[2];
              }
              if(i==8 && nodePhotos.friends.length>=4){
                coords[i].name = nodePhotos.friends[3];
              }
            }
              
              svgLinks = g.selectAll("line").data(links)
                      .enter().append("line").style("stroke", "white").style("stroke-width", 2);
              
              
              var photoNum = 6-nodePhotos.friends.length+1;

            circles = g.selectAll("circle").data(coords)
              .enter().append("circle")
              .attr("r", function(d){return d.r;})
              .attr("fill", function(d){return d.color;})
              .attr("stroke", function(d){return d.stroke;})
              .attr("stroke-width", 3)
              .attr("class", "node");
              
              if($scope.vkDataLoaded=="loaded"){

               clipPath = g.selectAll('clipPath').data(coords).enter().append('clipPath')
                 .attr('id', function(d) { return "clip" + d.index; });
              
              clipPathCircles = clipPath.append('circle').attr('r', function(d, i){return (d.r);})
                                      .attr('class', 'pathCircle');
              
              
              photos = g.selectAll("ciclePhoto").data(coords).enter().append("image")
                .attr('xlink:href', function(d, i){
                          if(d.name!=undefined)
                            return d.name.photo;
                          else
                            return "";
                        })
                        .attr('clip-path', function(d, i){ return "url(#clip" + d.index + ")"; })
                        .attr('width', function(d, i){return (d.r*2);})
                        .attr('height', function(d, i){return (d.r*2);})
                        .on('mouseover', function(d, i){
                 if(d.name && !dragged){
                tip.show(d);
              
                  if(!radiusBigger){
        
                          coords[d.index].r =  coords[d.index].r*1.4; 
                          
                    
                    for(var y=0;y<coords.length;y++){
                  d3.select(circles[0][y])
                  .attr('r', function(d, i){
                          return d.r;
                  });
                      d3.select(photos[0][y])
                  .attr('width', function(d, i){
                          return d.r*2;
                  })
                      .attr('height', function(d, i){
                          return d.r*2;
                  })
                        .attr('x', function(d){return (d.x-d.r);})
                      .attr('y', function(d){return (d.y-d.r);});
                      
                      d3.select(clipPathCircles[0][y])
                  .attr('r', function(d, i){
                          return (d.r);
                  });
                    }
                  
                      radiusBigger = true;
                  }

                }
              
              })
              .on('mouseout', function(d, i){
                if(d.name) {

                tip.hide();


                  if(radiusBigger){
                    
                              coords[d.index].r =  coords[d.index].r/1.4; 
                          
                    
                     for(var y=0;y<coords.length;y++){
                  d3.select(circles[0][y])
                  .attr('r', function(d, i){return d.r;});
                        d3.select(photos[0][y])
                  .attr('width', function(d, i){return d.r*2;})
                        .attr('height', function(d, i){return d.r*2;})
                        .attr('x', function(d){return (d.x-d.r);})
                      .attr('y', function(d){return (d.y-d.r);});
                       
                        d3.select(clipPathCircles[0][y])
                  .attr('r', function(d, i){return (d.r);});
                     }
                    
                      radiusBigger = false;
                      }  
              }
              });
              }
              
              if(isFirst){
                 circles.call(drag);
                if($scope.vkDataLoaded=="loaded")
                photos.call(drag);
                web.nodes(coords)
                .links(links)
                .start();
              }
            }
            
            function update(){
              d3.selectAll("circle.node").data(coords).attr("stroke", function(d){return d.stroke;})
                                    .attr("fill", function(d){return d.color;})
                                    .attr('r', function(d){return d.r;});
              
              web.nodes(coords)
                .links(links)
                .start();
            }
            
            function remove(isUpdate){
            if(isUpdate)  
            d3.selectAll("circle.node").data(coords).attr("stroke", function(d){return d.stroke;})
                                              .attr("fill", function(d){return d.color;});
            
              g.selectAll("circle.node").data(coords, function(d){return d.index;}).exit().remove();
              g.selectAll("image").data(coords, function(d){return d.index;}).exit().remove();
              g.selectAll("clippath").data(coords, function(d){return d.index;}).exit().remove();
             g.selectAll("line").data(links, function(d){return d.source.index+"-"+d.target.index;}).exit().remove();
              
              web.nodes(coords)
                .links(links)
                .start();
            }
            
            function changeBackground(bg){
                if(bg=='infected-text'){
                  $('.d3-tip').addClass('infected-tip');
                  $('.d3-tip').removeClass('carant-tip');
                  $('.d3-tip').removeClass('vaxed-tip');
                }
                else if(bg=='carant-text'){
                  $('.d3-tip').addClass('carant-tip');
                $('.d3-tip').removeClass('infected-tip');
                  $('.d3-tip').removeClass('vaxed-tip');
                }
                else if(bg=='vaxed-text'){
                  $('.d3-tip').addClass('vaxed-tip');
                $('.d3-tip').removeClass('infected-tip');
                  $('.d3-tip').removeClass('carant-tip');
                }
                else{
                  $('.d3-tip').removeClass('infected-tip');
                  $('.d3-tip').removeClass('carant-tip');
                  $('.d3-tip').removeClass('vaxed-tip');
                }
              }
              
              $scope.$on('tutorialEnd', function(){
                $scope.editor.nextStep();
              });
              
              firstRun = true;
            
          });
              });
        });
        }
      };
    }]);